﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Console_BounceBackHistory_Report
{
    class Program
    {
        static private String delimiter = "||";
        static Boolean error = false;
        static private String BounceBackHistoryOutputPath = ConfigurationManager.AppSettings.Get("BounceBackHistoryOutputPath") + "\\";
        static private String BounceBackHistoryLogFilePath = ConfigurationManager.AppSettings.Get("BounceBackHistoryLogFilePath") + "\\" + ConfigurationManager.AppSettings.Get("BounceBackHistoryLogFileName");
        //static private String misStartDate = ConfigurationManager.AppSettings.Get("misStartDate");
        //static private String misEndDate = ConfigurationManager.AppSettings.Get("misEndDate");
        static String genfileExt = "";
        static String genfileExtLog = "";
        static String Text = "";

        static void Main(string[] args)
        {
            error = false;
            //genfileExt = DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            genfileExt = ".csv";
            //genfileExtLog = DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            genfileExtLog = ".txt";
            //DateTime startDate = DateTime.MinValue;//dtpStart.Value;
            //DateTime endDate = DateTime.MaxValue;//dtpEnd.Value;
            DateTime startDate;
            DateTime endDate;
            Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console BounceBackHistory Report Start"));

            System.IO.StreamWriter logfile = new System.IO.StreamWriter(BounceBackHistoryLogFilePath + genfileExtLog, true, Encoding.UTF8);

            endDate = DateTime.Now;
            endDate = endDate.Date;
            startDate = endDate.AddDays(-1);
            endDate = endDate.AddSeconds(-1);

            if (args.Length == 2)
            {
                
                if (DateTime.TryParseExact(args[0],
                               "yyyyMMdd",
                               System.Globalization.CultureInfo.InvariantCulture,
                               System.Globalization.DateTimeStyles.None,
                               out startDate))
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "BounceBackHistory Start Date for Report: " + startDate);
                }
                else
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid BounceBackHistory Start Date format for Report ");
                    logfile.Write(Text);
                    logfile.Close();
                    Console.WriteLine(Text);
                    Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console BounceBackHistory Report End"));
                    System.Threading.Thread.Sleep(3000);
                    return;
                }

                if (DateTime.TryParseExact(args[1],
                               "yyyyMMdd",
                               System.Globalization.CultureInfo.InvariantCulture,
                               System.Globalization.DateTimeStyles.None,
                               out endDate))
                {
                    endDate = endDate.AddDays(1).AddSeconds(-1);
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "BounceBackHistory End Date for Report: " + endDate);

                }
                else
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid BounceBackHistory End Date format for Report ");
                    logfile.Write(Text);
                    logfile.Close();
                    Console.WriteLine(Text);
                    Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console BounceBackHistory Report End"));
                    System.Threading.Thread.Sleep(3000);
                    return;
                }

            }
            else if (args.Length == 1)
            {
                if (DateTime.TryParseExact(args[0],
                               "yyyyMMdd",
                               System.Globalization.CultureInfo.InvariantCulture,
                               System.Globalization.DateTimeStyles.None,
                               out startDate))
                {
                    endDate = startDate.AddDays(1).AddSeconds(-1);
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "BounceBackHistory Start Date for Report: " + startDate);
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "BounceBackHistory End Date for Report: " + endDate);
                }
                else
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid BounceBackHistory Start Date format for Report ");
                    logfile.Write(Text);
                    logfile.Close();
                    Console.WriteLine(Text);
                    Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console BounceBackHistory Report End"));
                    System.Threading.Thread.Sleep(3000);
                    return;
                }
            }
            else if (args.Length == 0)
            {
                // Last Month Report
                //date = date.AddMonths(-1);
                //int days = DateTime.DaysInMonth(date.Year, date.Month);
                //startDate = new DateTime(date.Year, date.Month, 1);
                //endDate = new DateTime(date.Year, date.Month, days);
                //endDate = endDate.AddDays(1).AddSeconds(-1);

                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "BounceBackHistory Start Date for Report: " + startDate);
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "BounceBackHistory End Date for Report: " + endDate);
            }
            else 
            {
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid Input Arguements format.");
                logfile.Write(Text);
                logfile.Close();
                Console.WriteLine(Text);
                Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console BounceBackHistory Report End"));
                System.Threading.Thread.Sleep(3000);
                return;
            }
            Int32 noOfRecord = 0;

            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Start Generate BounceBackHistory Report");
            noOfRecord = investmentReport(startDate, endDate);//saveFileDialog1.FileName);
            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Finish Generate BounceBackHistory Report");
            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   No. of record:{1} has been generated", Text, noOfRecord);
            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Please get the report on '{1}'", Text, BounceBackHistoryOutputPath + ConfigurationManager.AppSettings.Get("BounceBackHistoryOutputName") + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + genfileExt);
            logfile.Write(Text);
            logfile.Close();


            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Please get the log on '{1}'", Text, BounceBackHistoryLogFilePath + genfileExtLog);

            if (error)
            {
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Generate BounceBackHistory Report Completed with ERRORS", Text);
            }
            else
            {
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Generate BounceBackHistory Report Completed", Text);
            }
            Console.WriteLine(Text);
            Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console BounceBackHistory Report End"));
            System.Threading.Thread.Sleep(5000);
        }


        static private Int32 investmentReport(DateTime startDate, DateTime endDate)//, String fileName)
        {
            String csvData = "";
            DateTime statementSentDateTime;
            DateTime failureReportDateTime;
            DateTime smsSentDateTime;
            Int32 noOfRecord = 0;
            System.IO.StreamWriter file1 = new System.IO.StreamWriter(BounceBackHistoryOutputPath + ConfigurationManager.AppSettings.Get("BounceBackHistoryOutputName") + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + genfileExt, false, Encoding.UTF8);
            try
            {
                using (EstatementEntities entities = new EstatementEntities())
                {
                    var bounceBackHistoryData = entities.tbBounceBackHistories.Where(c => ((DateTime)c.FailureReportDateTime).CompareTo(startDate) >= 0 && ((DateTime)c.FailureReportDateTime).CompareTo(endDate) <= 0 );

                    //using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, false, Encoding.Unicode))
                    //{
                    #region " Header "
                    csvData = @"""ID""";
                    csvData = csvData + delimiter + @"""Statement ID""";
                    csvData = csvData + delimiter + @"""Statement Type""";
                    csvData = csvData + delimiter + @"""Statement Doc Type""";
                    csvData = csvData + delimiter + @"""RMID""";
                    csvData = csvData + delimiter + @"""Account Number""";
                    csvData = csvData + delimiter + @"""Email Address""";
                    csvData = csvData + delimiter + @"""Statement Sent Date Time""";
                    csvData = csvData + delimiter + @"""Failure Report Date Time""";
                    csvData = csvData + delimiter + @"""Failure Report Subject""";
                    csvData = csvData + delimiter + @"""Hard Soft Bounce Indicator""";
                    csvData = csvData + delimiter + @"""Preferred Language""";
                    csvData = csvData + delimiter + @"""Mobile Number""";
                    csvData = csvData + delimiter + @"""SMS Sent Date Time""";
                    csvData = csvData + delimiter + @"""SMS Content""";
                    csvData = csvData + delimiter + @"""SMS Email Bounce Back Indicator""";
                    csvData = csvData + delimiter + @"""First Time Bounce Back Indicator""";

                    file1.WriteLine(csvData);
                    #endregion

                    #region " Body "
                    foreach (var item in bounceBackHistoryData)
                    {
                        DateTime.TryParse(item.StatementSentDateTime.ToString(), out statementSentDateTime);
                        DateTime.TryParse(item.FailureReportDateTime.ToString(), out failureReportDateTime);
                        DateTime.TryParse(item.SMSSentDateTime.ToString(), out smsSentDateTime);

                        csvData = "\"" + item.ID;      //ID
                        csvData = csvData + "\"" + delimiter + "\"" + item.StatementID;      //Statement ID
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.StatementType)).Trim();      //Statement Type
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.StatementDocType)).Trim();      //Statement Doc Type
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.RMID)).Trim();      //RMID
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.AccountNumber)).Trim();      //Account Number
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.EmailAddress)).Trim();      //Email Address
                        csvData = csvData + "\"" + delimiter + "\"" + (item.StatementSentDateTime != null ? statementSentDateTime.ToString("yyyyMMddHHmmss") : "");      //Statement Sent Date Time
                        csvData = csvData + "\"" + delimiter + "\"" + (item.FailureReportDateTime != null ? failureReportDateTime.ToString("yyyyMMddHHmmss") : "");      //Failure Report Date Time
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.FailureReportSubject)).Trim();      //Failure Report Subject
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.HardSoftBounceIndicator)).Trim();      //Hard Soft Bounce Indicator
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.PreferredLanguage)).Trim();      //Preferred Language
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.MobileNumber)).Trim();      //Mobile Number
                        csvData = csvData + "\"" + delimiter + "\"" + (item.SMSSentDateTime != null ? smsSentDateTime.ToString("yyyyMMddHHmmss") : "");      //SMS Sent Date Time
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.SMSContent)).Trim();      //SMS Content
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.SMSEmailBounceBackIndicator)).Trim();      //SMS Email BounceBack Indicator
                        csvData = csvData + "\"" + delimiter + "\"" + (trimming(item.FirstTimeBounceBackIndicator)).Trim();      //First Time BounceBack Indicator

                        file1.WriteLine(csvData + "\"");
                        noOfRecord++;
                    }

                    #endregion
                    //}
                }
            }
            catch (Exception e)
            {
                error = true;
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Error Generate BounceBackHistory Report Error: " + e.ToString() + ((e.InnerException == null) ? "" : " (InnerException: " + e.InnerException.ToString() + ")"));
            }
            file1.Close();

            return noOfRecord;
        }

        static private String trimming(String str)
        {
            if (String.IsNullOrEmpty(str))
                return "";
            else
                return str.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
        }


    }
}
