﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;

namespace bounceBackNoticeApp
{
    public enum logType { Info, Error, Warning };

    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program bounceBackNoticeApp Start"));
                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program bounceBackNoticeApp Start");

                EstatementEntities entities = new EstatementEntities();
                List<bounceBackInterface> bounceBackList = new List<bounceBackInterface> { };

                SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["DBConnectString"]);
                conn.Open();
                int EMailValidInputCount = 0;
                int SMSValidInputCount = 0;
                SqlDataReader reader = null;


                if (args.Length != 0 && args[0] == "connection")
                {

                    if (args.Length != 2)
                    {
                        Console.WriteLine("Incorrect no. of args. Command format should be EStatementArrivalEmail mobileNo <mobileNo(Int)> <templateID> <eStatementID(Int)> ");
                        return;
                    }
                    int StatementID = int.Parse(args[1]);
                    Console.WriteLine("Statement ID: " + StatementID);

                    var testRecord = entities.tbNotifications.Where(a => a.StatementID == StatementID).FirstOrDefault();

                    if (testRecord != null)
                    {
                        if (!String.IsNullOrEmpty(testRecord.EMailAddress))
                            Console.WriteLine("Email: " + testRecord.EMailAddress);
                        else
                        {
                            Console.WriteLine("Record Email field is empty");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Record Not Found");
                    }

                    return;
                }
                string[] emailFileList = Directory.GetFiles(Properties.Settings.Default.EmailInputPath, Properties.Settings.Default.EmailInputFilename);
                if (emailFileList.Length == 0)
                {
                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Empty Email Input file"));
                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Empty Email Input file");
                }
                else
                {
                    foreach (String filename in emailFileList)
                    {
                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Email Input file: " + filename));
                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Email Input file: " + filename);
                        int lineCount = 0;
                        int totalLineCount = 0;

                        var lines = System.IO.File.ReadAllLines(filename);
                        //var record = new tbNotification();
                        foreach (var line in lines)
                        //   foreach (string line in lines)
                        {
                            try
                            {
                                ++totalLineCount;

                                if (totalLineCount == 1)
                                {
                                    continue;
                                }

                                if (!String.IsNullOrEmpty(line) && totalLineCount != lines.Length)
                                {
                                    ++lineCount;
                                }


                                if (totalLineCount == lines.Length)
                                {
                                    if (line.Length < 37 || !line.Substring(0, 36).Equals("Total number of bounce-back emails: ") || int.Parse(line.Substring(36)) != lineCount)
                                    {
                                        Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] EMail Input Line Count: " + int.Parse(line.Substring(36)) + " Actual Line Count: " + lineCount));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] EMail Input Line Count: " + int.Parse(line.Substring(36)) + " Actual Line Count: " + lineCount);
                                    }
                                    break;
                                }
                                if (!String.IsNullOrEmpty(line))
                                {
                                    Char delimiter = '"';
                                    String[] substrings = line.Split(delimiter);

                                    if (substrings.Length != 15)
                                    {
                                        Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] EMail Input Line " + lineCount + ": " + line.ToString() + " Reason: " + "Invalid Input Line"));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] EMail Input Line " + lineCount + ": " + line.ToString() + " Reason: " + "Invalid Input Line");
                                        continue;
                                    }
                                    else
                                    {
                                        EMailValidInputCount++;
                                    }

                                    bounceBackEmail email = new bounceBackEmail(substrings);
                                    bounceBackList.Add(email);
                                }
                            }
                            catch (Exception ex)
                            {
                                Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Email Input Line " + lineCount + ": " + line.ToString() + " Reason: " + ex.Message.ToString()));
                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Email Input Line " + lineCount + ": " + line.ToString() + " Reason: " + ex.Message.ToString());

                            }
                        }

                        try
                        {
                            //move file
                            string emailInputFileName = Path.GetFileName(filename);
                            System.IO.File.Move(filename, Properties.Settings.Default.EmailBackupPath + DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + emailInputFileName);
                            Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Email Input File: " + filename + " Move to Backup Folder: " + Properties.Settings.Default.EmailBackupPath));
                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Email Input File: " + filename + " Move to Backup Folder: " + Properties.Settings.Default.EmailBackupPath);
                        }
                        catch (Exception ex)
                        {
                            Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Email Input File Move Error: " + ex.Message.ToString()));
                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Email Input File Move Error: " + ex.Message.ToString());
                        }
                    }
                }

                string[] smsFileList = Directory.GetFiles(Properties.Settings.Default.SMSInputPath, Properties.Settings.Default.SMSInputFilename);
                if (smsFileList.Length == 0)
                {
                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Empty SMS Input file"));
                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Empty SMS Input file");
                }
                else
                {
                    foreach (String filename in smsFileList)
                    {
                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] SMS Input file: " + Properties.Settings.Default.SMSInputPath));
                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] SMS Input file: " + Properties.Settings.Default.SMSInputPath);
                        int lineCount = 0;
                        int totalLineCount = 0;

                        var lines = System.IO.File.ReadAllLines(filename);
                        //var record = new tbNotification();
                        foreach (var line in lines)
                        //   foreach (string line in lines)
                        {
                            try
                            {
                                ++totalLineCount;

                                if (totalLineCount == 1)
                                {
                                    continue;
                                }

                                if (!String.IsNullOrEmpty(line) && totalLineCount != lines.Length)
                                {
                                    ++lineCount;
                                }


                                if (totalLineCount == lines.Length)
                                {
                                    if (line.Length < 11 || int.Parse(line.Substring(0, 10)) != lineCount)
                                    {
                                        Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] SMS Input Line Count: " + int.Parse(line.Substring(33)) + " Actual Line Count: " + lineCount));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] SMS Input Line Count: " + int.Parse(line.Substring(33)) + " Actual Line Count: " + lineCount);
                                    }
                                    break;
                                }

                                if (!String.IsNullOrEmpty(line))
                                {
                                    String[] substrings;

                                    if (line.Length < 75)
                                    {
                                        Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] SMS Input Line " + lineCount + ": " + line.ToString() + " Reason: " + "Invalid Input Line Format"));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] SMS Input Line " + lineCount + ": " + line.ToString() + " Reason: " + "Invalid Input Line Format");
                                        continue;
                                    }
                                    else
                                    {
                                        String appID = line.Substring(0, 20).Trim();
                                        String refNo = line.Substring(20, 20).Trim();
                                        String smsStatus = line.Substring(40, 1).Trim();
                                        String lastUpdateDatetime = line.Substring(41, 14).Trim();
                                        String appRef = line.Substring(55, 20).Trim();


                                        switch (smsStatus)
                                        {
                                            case "1":
                                            case "8":
                                            case "A":
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] SMS Status: 1/8/A  Ref Number: " + refNo);
                                                break;
                                            default:
                                                substrings = new string[] { appID, refNo, smsStatus, lastUpdateDatetime, appRef };
                                                SMSValidInputCount++;
                                                bounceBackSMS sms = new bounceBackSMS(substrings);
                                                bounceBackList.Add(sms);
                                                break;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] SMS Input Line " + lineCount + ": " + line.ToString() + " Reason: " + ex.Message.ToString()));
                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] SMS Input Line " + lineCount + ": " + line.ToString() + " Reason: " + ex.Message.ToString());

                            }
                        }
                        try
                        {
                            //move file
                            string smsInputFileName = Path.GetFileName(filename);
                            System.IO.File.Move(filename, Properties.Settings.Default.SMSBackupPath + DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + smsInputFileName);
                            Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] SMS Input File: " + filename + " Move to Backup Folder: " + Properties.Settings.Default.SMSBackupPath));
                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] SMS Input File: " + filename + " Move to Backup Folder: " + Properties.Settings.Default.SMSBackupPath);
                        }
                        catch (Exception ex)
                        {
                            Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] SMS Input File Move Error: " + ex.Message.ToString()));
                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] SMS Input File Move Error: " + ex.Message.ToString());
                        }
                    }
                }

                int triggerDateTimeOffset;
                bool isNumericTriggerDateTimeOffset = int.TryParse(Properties.Settings.Default.triggerDateTimeOffset, out triggerDateTimeOffset);

                if (isNumericTriggerDateTimeOffset)
                {
                    if (triggerDateTimeOffset >= 0)
                    {
                        //Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: "));
                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Trigger Date Time Offset: " + triggerDateTimeOffset);
                    }
                    else 
                    {
                        //Default trigger date time offset 24 hours
                        triggerDateTimeOffset = 24;
                        //Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: "));
                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invalid Trigger Date Time Offset Input(Value < 0), Default Value: 24 Hours.");
                    }
                }
                else 
                {
                    //Default trigger date time offset 24 hours
                    triggerDateTimeOffset = 24;
                    //Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: "));
                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invalid Trigger Date Time Offset Input(Not Numeric Value), Default Value: 24 Hours.");
                           
                }

                foreach (bounceBackInterface bounceBackObj in bounceBackList)
                {
                    if (bounceBackObj.GetType() == typeof(bounceBackEmail))
                    {
                        //System.Console.WriteLine("Email");
                        //System.Console.WriteLine(bounceBackObj.deliveredDate);
                        //System.Console.WriteLine(bounceBackObj.subject);
                        //System.Console.WriteLine(bounceBackObj.sendTo);
                        //System.Console.WriteLine(bounceBackObj.failureReason);
                        //System.Console.WriteLine(bounceBackObj.messageId);
                        //System.Console.WriteLine(bounceBackObj.SMTPEnvid);
                        //System.Console.WriteLine(bounceBackObj.status);

                        var hardBounceRecord = entities.tbBounceBackKeywords.Where(a => bounceBackObj.failureReason.Contains(a.Description)).Where(a => "Hard".Contains(a.BounceBackType)).FirstOrDefault();
                        if (hardBounceRecord == null) 
                        {
                            hardBounceRecord = entities.tbBounceBackKeywords.Where(a => bounceBackObj.subject.Contains(a.Description)).Where(a => "Hard".Contains(a.BounceBackType)).FirstOrDefault();
                        }
                        Boolean HS = !(hardBounceRecord == null);
                        String[] substrings = bounceBackObj.subject.Split(new string[] { "[Ref. ", "]" }, StringSplitOptions.None);

                        int StatementID = -1;
                        int SBSID = -1;
                        bool isNumeric = false;
                        
                        //int statementID = Int32.Parse(substrings.GetValue(10).ToString());
                        if (substrings.Length > 2)
                        {
                            if (substrings.GetValue(1).ToString().Substring(0, 1).Equals("S"))
                            {
                                isNumeric = int.TryParse(substrings.GetValue(1).ToString().Substring(1), out SBSID);
                            }
                            else
                            {
                                isNumeric = int.TryParse(substrings.GetValue(1).ToString(), out StatementID);
                            }
                        }

                        if (!isNumeric)
                        {
                            //Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Invalid Statement ID in Input File Record: " + bounceBackObj.deliveredDate + ", " + bounceBackObj.subject + ", " + bounceBackObj.sendTo + ", " + bounceBackObj.failureReason + ", " + bounceBackObj.messageId + ", " + bounceBackObj.SMTPEnvid + ", " + bounceBackObj.status + " Reason: Invalid Statment ID Input value or Out of Integer value range (-2,147,483,648 to 2,147,483,647)"));
                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Invalid Statement ID in Input File Record: " + bounceBackObj.deliveredDate + ", " + bounceBackObj.subject + ", " + bounceBackObj.sendTo + ", " + bounceBackObj.failureReason + ", " + bounceBackObj.messageId + ", " + bounceBackObj.SMTPEnvid + ", " + bounceBackObj.status + " Reason: Invalid Statment ID Input value or Out of Integer value range (-2,147,483,648 to 2,147,483,647)");
                            continue;
                        }

                        DateTime emailDelayLimit = DateTime.Now;
                        int emailDelayLimitOffset;
                        bool isNumericEmailDelayLimitOffset = int.TryParse(Properties.Settings.Default.emailBouncebackDelayLimit, out emailDelayLimitOffset);

                        if (isNumericEmailDelayLimitOffset)
                        {
                            if (emailDelayLimitOffset >= 0)
                            {
                                emailDelayLimit = emailDelayLimit.AddHours(-emailDelayLimitOffset);
                                //Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: "));
                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Email Delay Limit: " + emailDelayLimit + " Hours: " + emailDelayLimitOffset);
                            }
                            else 
                            {
                                //Default email delay limit 24 hours
                                emailDelayLimit = emailDelayLimit.AddHours(-24);
                                //Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: "));
                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invalid Email Delay Limit Input. Default 24 Hours: " + emailDelayLimit);
                            }
                        }
                        else 
                        {
                            //Default email delay limit 24 hours
                            emailDelayLimit = emailDelayLimit.AddHours(-24);
                            //Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: "));
                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invalid Email Delay Limit Input. Default 24 Hours: " + emailDelayLimit);
                        }


                        var recordNormal = entities.tbNotifications.Where(a => a.StatementID.Equals(StatementID)).Where(b => b.SentDateTime > emailDelayLimit).FirstOrDefault();


                        var recordSBS = entities.tbNotificationSBS.Where(a => a.ID.Equals(SBSID)).Where(b => b.SentDateTime > emailDelayLimit).FirstOrDefault();



                        if (recordNormal != null && StatementID != -1)
                        {
                            if (recordNormal.Status == "S")
                            {
                                //conn.Open();

                                //No PK in tbCustomerEMail, it can't use ADO
                                //It need to use SQL cmd to select
                                SqlCommand cmdSelect = new SqlCommand("SELECT TOP 1 EMailAddress, MobileNumber, AcctStatus, AcctStatusLastUpdDt, PreferredLanguage FROM tbCustomerEMail WHERE RMID = @RMID", conn);

                                cmdSelect.Parameters.AddWithValue("@RMID", recordNormal.RMID);

                                reader = cmdSelect.ExecuteReader();


                                if (reader.HasRows)
                                {
                                    tbMISReportEmail MISReportEmail = new tbMISReportEmail();
                                    tbBounceBackHistory bounceBackHistoryEmail = new tbBounceBackHistory();
                                    while (reader.Read())
                                    {
                                        //Add Bounce Back History (Email)
                                        bounceBackHistoryEmail.StatementID = StatementID;
                                        bounceBackHistoryEmail.StatementType = recordNormal.StatementType;
                                        bounceBackHistoryEmail.StatementDocType = recordNormal.statementFreq;
                                        bounceBackHistoryEmail.RMID = recordNormal.RMID;
                                        bounceBackHistoryEmail.AccountNumber = recordNormal.DisplayAccountNumber;
                                        bounceBackHistoryEmail.EmailAddress = recordNormal.EMailAddress;
                                        bounceBackHistoryEmail.StatementSentDateTime = recordNormal.SentDateTime;
                                        bounceBackHistoryEmail.FailureReportDateTime = DateTime.Now;

                                        if (bounceBackObj.subject.Length < 150)
                                        {
                                            bounceBackHistoryEmail.FailureReportSubject = bounceBackObj.subject;
                                        }
                                        else 
                                        {
                                            bounceBackHistoryEmail.FailureReportSubject = bounceBackObj.subject.Substring(0, 149);
                                            Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Subject Content Out of Database Input Range, Content truncated to 149 characters for reference "));
                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Subject Content Out of Database Input Range, Content truncated to 149 characters for reference ");
                                        }

                                        if (bounceBackObj.failureReason.Length <= 200)
                                        {
                                            bounceBackHistoryEmail.FailureReportSubject = bounceBackHistoryEmail.FailureReportSubject + "|" + bounceBackObj.failureReason;
                                        }
                                        else
                                        {
                                            bounceBackHistoryEmail.FailureReportSubject = bounceBackHistoryEmail.FailureReportSubject + "|" + bounceBackObj.failureReason.Substring(0, 200);
                                            Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Failure Reason Content Out of Database Input Range, Content truncated to 200 characters for reference "));
                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Failure Reason Content Out of Database Input Range, Content truncated to 200 characters for reference ");
                                        }

                                        bounceBackHistoryEmail.HardSoftBounceIndicator = HS.Equals(true) ? "H" : "S";
                                        bounceBackHistoryEmail.PreferredLanguage = (string)reader["PreferredLanguage"];
                                        bounceBackHistoryEmail.MobileNumber = recordNormal.MobileNo;
                                        bounceBackHistoryEmail.SMSEmailBounceBackIndicator = "E";

                                        if (recordNormal.bounceBackCount.Equals(null) || recordNormal.bounceBackCount.Equals(0))
                                        {
                                            bounceBackHistoryEmail.FirstTimeBounceBackIndicator = "Y";
                                        }
                                        else 
                                        {
                                            bounceBackHistoryEmail.FirstTimeBounceBackIndicator = "N"; 
                                        }

                                        //Soft Case Handle
                                        if (bounceBackHistoryEmail.HardSoftBounceIndicator.Equals("S")) {
                                            bounceBackHistoryEmail.FirstTimeBounceBackIndicator = "N"; 
                                        }

                                        //Add Email MIS Report
                                        MISReportEmail.StatementID = StatementID;
                                        MISReportEmail.StatementType = recordNormal.StatementType;
                                        MISReportEmail.RMID = recordNormal.RMID;
                                        MISReportEmail.AccountNumber = recordNormal.DisplayAccountNumber;
                                        MISReportEmail.EmailAddress = recordNormal.EMailAddress;
                                        MISReportEmail.StatementSentDate = recordNormal.SentDateTime;
                                        MISReportEmail.FailureReportDateTime = DateTime.Now;

                                        if (bounceBackObj.subject.Length < 100)
                                        {
                                            MISReportEmail.FailureReportSubject = bounceBackObj.subject;
                                        }
                                        else
                                        {
                                            MISReportEmail.FailureReportSubject = bounceBackObj.subject.Substring(0, 95);
                                            Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Subject Content Out of Database Input Range, Content truncated to 95 characters for reference "));
                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Subject Content Out of Database Input Range, Content truncated to 95 characters for reference ");
                                        }
                                        
                                        MISReportEmail.HardSoftBounce = HS.Equals(true) ? "Hard" : "Soft";
                                        MISReportEmail.MobileNumber = recordNormal.MobileNo;
                                        MISReportEmail.PreferredLanguage = (string)reader["PreferredLanguage"];
                                        MISReportEmail.UNEReferenceNumber = recordNormal.UNEReferenceNumber;
                                    }
                                    entities.AddTotbBounceBackHistories(bounceBackHistoryEmail);
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert Bounce Back History (EMail) table Success "));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert Bounce Back History (EMail) table Success  ");

                                    entities.AddTotbMISReportEmails(MISReportEmail);
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert MIS table Success "));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert MIS table Success  ");
                                }
                                else
                                {
                                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert MIS table Failed" + " Reason: Record Not Found in tbCustomerEMail"));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert MIS table Failed" + " Reason: Record Not Found in tbCustomerEMail");
                                }

                                if (!HS)
                                {
                                    //Soft Bounce
                                    recordNormal.Status = "F";
                                    recordNormal.HS = false;
                                    recordNormal.LastStatusUpdateTime = System.DateTime.Now;

                                    entities.SaveChanges();
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Status: Soft Bounce Back"));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Status: Soft Bounce Back");
                                    continue;//skip
                                }

                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Status: Hard Bounce Back"));
                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Status: Hard Bounce Back");

                                // S is first time
                                // R is under bounceback stage
                                recordNormal.HS = HS;

                                if (recordNormal.bounceBackStatus == null)
                                {
                                    recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.FIRSTEMAIL;
                                }

                                if (recordNormal.StatementType.Equals("InfoCast") || recordNormal.StatementType.Equals("CF") || recordNormal.StatementType.Equals("PB"))
                                {
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " StatementType:" + recordNormal.StatementType + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Statement Type: Invest"));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " StatementType:" + recordNormal.StatementType + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Statement Type: Invest");
                                }
                                else
                                {
                                    if (recordNormal.bounceBackCount.Equals(null))
                                    {
                                        recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.THIRDEMAIL;
                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " StatementType:" + recordNormal.StatementType + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Statement Type: Non-Invest"));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " StatementType:" + recordNormal.StatementType + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Statement Type: Non-Invest");
                                    }
                                }

                                if (recordNormal.bounceBackCount == null)
                                {
                                    recordNormal.bounceBackCount = 0;
                                }
                                recordNormal.TriggerDateTime = System.DateTime.Now;

                                switch (recordNormal.bounceBackStatus.Trim())
                                {
                                    case BOUNCEBACK_STATUS.FIRSTEMAIL:
                                        // 1st Email Rebound and send 2nd Email
                                        recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.SECONDEMAIL;
                                        recordNormal.bounceBackCount += 1;
                                        recordNormal.Status = "R";
                                        recordNormal.LastStatusUpdateTime = System.DateTime.Now;

                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.FIRSTEMAIL + "->" + BOUNCEBACK_STATUS.SECONDEMAIL + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.FIRSTEMAIL + "->" + BOUNCEBACK_STATUS.SECONDEMAIL + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime);

                                        break;
                                    case BOUNCEBACK_STATUS.SECONDEMAIL:
                                        // 2nd Email Rebound and send 3rd Email
                                        recordNormal.TriggerDateTime = System.DateTime.Now.AddHours(triggerDateTimeOffset);
                                        //DateTime triggerDateTime = System.DateTime.Now.AddDays(1);
                                        //record.TriggerDateTime = new DateTime(triggerDateTime.Year, triggerDateTime.Month, triggerDateTime.Day, 9, 0, 0, 0, triggerDateTime.Kind);
                                        //Config File?
                                        recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.THIRDEMAIL;
                                        recordNormal.bounceBackCount += 1;
                                        recordNormal.Status = "R";
                                        recordNormal.LastStatusUpdateTime = System.DateTime.Now;

                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.SECONDEMAIL + "->" + BOUNCEBACK_STATUS.THIRDEMAIL + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.SECONDEMAIL + "->" + BOUNCEBACK_STATUS.THIRDEMAIL + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime);

                                        break;
                                    case BOUNCEBACK_STATUS.THIRDEMAIL:
                                        // 3rd Email Rebound and send 1st SMS
                                        if (!String.IsNullOrEmpty(recordNormal.MobileNo))
                                        {//MobileNo not Null
                                            recordNormal.bounceBackCount += 1;
                                            recordNormal.Status = "R";
                                            recordNormal.LastStatusUpdateTime = System.DateTime.Now;

                                            if (recordNormal.StatementType.Equals("InfoCast") || recordNormal.StatementType.Equals("CF") || recordNormal.StatementType.Equals("PB"))
                                            {
                                                recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.FIRSTSMS;

                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDEMAIL + "->" + BOUNCEBACK_STATUS.FIRSTSMS + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime));
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDEMAIL + "->" + BOUNCEBACK_STATUS.FIRSTSMS + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime);
                                            }
                                            else 
                                            {
                                                recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.NonInvestSMSOTP;

                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> " + BOUNCEBACK_STATUS.NonInvestSMSOTP + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime));
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> " + BOUNCEBACK_STATUS.NonInvestSMSOTP + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime);
                                            }
                                        }
                                        else
                                        {//MobileNo Null
                                            recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.PaperStatment;
                                            recordNormal.bounceBackCount += 1;
                                            recordNormal.Status = "F";
                                            recordNormal.LastStatusUpdateTime = System.DateTime.Now;

                                            if (recordNormal.bounceBackCount == 1)
                                            {
                                                recordNormal.bounceBackStatus = "NIMM";
                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Non Invest(Missing Mobile) Case StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> NIMM" + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> F"));
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Non Invest(Missing Mobile) Case StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> NIMM" + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> F");
                                            }
                                            else
                                            {
                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invest(Missing Mobile) Case StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDEMAIL + "->" + BOUNCEBACK_STATUS.PaperStatment + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> F"));
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invest(Missing Mobile) Case StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDEMAIL + "->" + BOUNCEBACK_STATUS.PaperStatment + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> F");

                                                var statementRecord = entities.tbStatements.Where(a => a.ID.Equals(recordNormal.StatementID)).FirstOrDefault();
                                                if (statementRecord != null)
                                                {
                                                    var accountListRecord = entities.tbAccountLists.Where(b => b.ID.Equals(statementRecord.AccountID)).FirstOrDefault();
                                                    if (accountListRecord != null)
                                                    {
                                                        if (accountListRecord.RegistrationStatus.Equals("S"))
                                                        {
                                                            if (!Properties.Settings.Default.SkipChangePS.Equals("Y"))
                                                            {
                                                                accountListRecord.RegistrationStatus = "U";
                                                                accountListRecord.LastUpdate = System.DateTime.Now;
                                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbAccountLists table= " + "Registration Status: S -> U"));
                                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbAccountLists table= " + "Registration Status: S -> U");
                                                            }
                                                            else 
                                                            {
                                                                //Console.WriteLine("SkipChangePS");
                                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip All eStatement To Paper Statement Case= StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber));
                                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip All eStatement To Paper Statement Case= StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber);
                                                            }
                                                            //Add Subscription Status Update
                                                            tbSubscriptionStatusUpdate subscriptionStatusUpdate = new tbSubscriptionStatusUpdate();
                                                            subscriptionStatusUpdate.ActionDateTime = DateTime.Now;
                                                            subscriptionStatusUpdate.RMID = accountListRecord.RMID;
                                                            subscriptionStatusUpdate.AccountType = accountListRecord.StatementType;
                                                            subscriptionStatusUpdate.AccountNumber = accountListRecord.DisplayAccountNumber;
                                                            subscriptionStatusUpdate.Action = "B";
                                                            entities.AddTotbSubscriptionStatusUpdates(subscriptionStatusUpdate);

                                                            Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbSubscriptionStatusUpdate table"));
                                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbSubscriptionStatusUpdate table");

                                                        }
                                                        else
                                                        {
                                                            Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " tbAccountLists Registration Status is U already"));
                                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " tbAccountLists Registration Status is U already");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbAccountLists table, Cannot Update Registration Status. StatementID: " + StatementID));
                                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbAccountLists table, Cannot Update Registration Status. StatementID: " + StatementID);
                                                    }
                                                }
                                                else
                                                {
                                                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbStatements table, Cannot Update Registration Status. StatementID: " + StatementID));
                                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbStatements table, Cannot Update Registration Status. StatementID: " + StatementID);
                                                }
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                //conn.Close();
                            }
                            else
                            { // 
                            }
                        }
                        else if (recordSBS != null && SBSID != -1)
                        {
                            if (recordSBS.Status == "S")
                            {
                                //conn.Open();

                                //No PK in tbCustomerEMail, it can't use ADO
                                //It need to use SQL cmd to select
                                SqlCommand cmdSelect = new SqlCommand("SELECT TOP 1 EMailAddress, MobileNumber, AcctStatus, AcctStatusLastUpdDt, PreferredLanguage FROM tbCustomerEMail WHERE RMID = @RMID", conn);

                                cmdSelect.Parameters.AddWithValue("@RMID", recordSBS.RMID);

                                reader = cmdSelect.ExecuteReader();


                                if (reader.HasRows)
                                {
                                    tbMISReportEmail MISReportEmail = new tbMISReportEmail();
                                    tbBounceBackHistory bounceBackHistoryEmail = new tbBounceBackHistory();
                                    while (reader.Read())
                                    {
                                        //Add Bounce Back History (Email)
                                        bounceBackHistoryEmail.StatementID = recordSBS.StatementID;
                                        bounceBackHistoryEmail.StatementType = recordSBS.StatementType;
                                        bounceBackHistoryEmail.StatementDocType = recordSBS.statementFreq;
                                        bounceBackHistoryEmail.RMID = recordSBS.RMID;
                                        bounceBackHistoryEmail.AccountNumber = recordSBS.DisplayAccountNumber;
                                        bounceBackHistoryEmail.EmailAddress = recordSBS.EMailAddress;
                                        bounceBackHistoryEmail.StatementSentDateTime = recordSBS.SentDateTime;
                                        bounceBackHistoryEmail.FailureReportDateTime = DateTime.Now;

                                        if (bounceBackObj.subject.Length < 150)
                                        {
                                            bounceBackHistoryEmail.FailureReportSubject = bounceBackObj.subject;
                                        }
                                        else
                                        {
                                            bounceBackHistoryEmail.FailureReportSubject = bounceBackObj.subject.Substring(0, 149);
                                            Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Subject Content Out of Database Input Range, Content truncated to 149 characters for reference "));
                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Subject Content Out of Database Input Range, Content truncated to 149 characters for reference ");
                                        }

                                        if (bounceBackObj.failureReason.Length <= 200)
                                        {
                                            bounceBackHistoryEmail.FailureReportSubject = bounceBackHistoryEmail.FailureReportSubject + "|" + bounceBackObj.failureReason;
                                        }
                                        else
                                        {
                                            bounceBackHistoryEmail.FailureReportSubject = bounceBackHistoryEmail.FailureReportSubject + "|" + bounceBackObj.failureReason.Substring(0, 200);
                                            Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Failure Reason Content Out of Database Input Range, Content truncated to 200 characters for reference "));
                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Failure Reason Content Out of Database Input Range, Content truncated to 200 characters for reference ");
                                        }

                                        bounceBackHistoryEmail.HardSoftBounceIndicator = HS.Equals(true) ? "H" : "S";
                                        bounceBackHistoryEmail.PreferredLanguage = (string)reader["PreferredLanguage"];
                                        bounceBackHistoryEmail.MobileNumber = recordSBS.MobileNo;
                                        bounceBackHistoryEmail.SMSEmailBounceBackIndicator = "E";

                                        if (recordSBS.bounceBackCount.Equals(null) || recordSBS.bounceBackCount.Equals(0))
                                        {
                                            bounceBackHistoryEmail.FirstTimeBounceBackIndicator = "Y";
                                        }
                                        else
                                        {
                                            bounceBackHistoryEmail.FirstTimeBounceBackIndicator = "N";
                                        }

                                        //Soft Case Handle
                                        if (bounceBackHistoryEmail.HardSoftBounceIndicator.Equals("S"))
                                        {
                                            bounceBackHistoryEmail.FirstTimeBounceBackIndicator = "N";
                                        }

                                        //Add Email MIS Report
                                        MISReportEmail.StatementID = recordSBS.StatementID;
                                        MISReportEmail.StatementType = recordSBS.StatementType;
                                        MISReportEmail.RMID = recordSBS.RMID;
                                        MISReportEmail.AccountNumber = recordSBS.DisplayAccountNumber;
                                        MISReportEmail.EmailAddress = recordSBS.EMailAddress;
                                        MISReportEmail.StatementSentDate = recordSBS.SentDateTime;
                                        MISReportEmail.FailureReportDateTime = DateTime.Now;

                                        if (bounceBackObj.subject.Length < 100)
                                        {
                                            MISReportEmail.FailureReportSubject = bounceBackObj.subject;
                                        }
                                        else
                                        {
                                            MISReportEmail.FailureReportSubject = bounceBackObj.subject.Substring(0, 95);
                                            Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Subject Content Out of Database Input Range, Content truncated to 95 characters for reference "));
                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Subject Content Out of Database Input Range, Content truncated to 95 characters for reference ");
                                        }

                                        MISReportEmail.HardSoftBounce = HS.Equals(true) ? "Hard" : "Soft";
                                        MISReportEmail.MobileNumber = recordSBS.MobileNo;
                                        MISReportEmail.PreferredLanguage = (string)reader["PreferredLanguage"];
                                        MISReportEmail.UNEReferenceNumber = recordSBS.UNEReferenceNumber;
                                    }
                                    entities.AddTotbBounceBackHistories(bounceBackHistoryEmail);
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert Bounce Back History (EMail) table Success "));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert Bounce Back History (EMail) table Success  ");

                                    entities.AddTotbMISReportEmails(MISReportEmail);
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert MIS table Success "));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert MIS table Success  ");
                                }
                                else
                                {
                                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert MIS table Failed" + " Reason: Record Not Found in tbCustomerEMail"));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert MIS table Failed" + " Reason: Record Not Found in tbCustomerEMail");
                                }

                                if (!HS)
                                {
                                    //Soft Bounce
                                    recordSBS.Status = "F";
                                    recordSBS.HS = false;
                                    recordSBS.LastStatusUpdateTime = System.DateTime.Now;

                                    entities.SaveChanges();
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Status: Soft Bounce Back"));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Status: Soft Bounce Back");
                                    continue;//skip
                                }

                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Status: Hard Bounce Back"));
                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Status: Hard Bounce Back");

                                // S is first time
                                // R is under bounceback stage
                                recordSBS.HS = HS;

                                if (recordSBS.bounceBackStatus == null)
                                {
                                    recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.FIRSTEMAIL;
                                }

                                if (recordSBS.StatementType.Equals("InfoCast") || recordSBS.StatementType.Equals("CF") || recordSBS.StatementType.Equals("PB"))
                                {
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " StatementType:" + recordSBS.StatementType + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Statement Type: Invest"));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " StatementType:" + recordSBS.StatementType + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Statement Type: Invest");
                                }
                                else
                                {
                                    if (recordSBS.bounceBackCount.Equals(null))
                                    {
                                        recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.THIRDEMAIL;
                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " StatementType:" + recordSBS.StatementType + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Statement Type: Non-Invest"));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " StatementType:" + recordSBS.StatementType + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Statement Type: Non-Invest");
                                    }
                                }

                                if (recordSBS.bounceBackCount == null)
                                {
                                    recordSBS.bounceBackCount = 0;
                                }
                                recordSBS.TriggerDateTime = System.DateTime.Now;

                                switch (recordSBS.bounceBackStatus.Trim())
                                {
                                    case BOUNCEBACK_STATUS.FIRSTEMAIL:
                                        // 1st Email Rebound and send 2nd Email
                                        recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.SECONDEMAIL;
                                        recordSBS.bounceBackCount += 1;
                                        recordSBS.Status = "R";
                                        recordSBS.LastStatusUpdateTime = System.DateTime.Now;

                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.FIRSTEMAIL + "->" + BOUNCEBACK_STATUS.SECONDEMAIL + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.FIRSTEMAIL + "->" + BOUNCEBACK_STATUS.SECONDEMAIL + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime);

                                        break;
                                    case BOUNCEBACK_STATUS.SECONDEMAIL:
                                        // 2nd Email Rebound and send 3rd Email
                                        recordSBS.TriggerDateTime = System.DateTime.Now.AddHours(triggerDateTimeOffset);
                                        //DateTime triggerDateTime = System.DateTime.Now.AddDays(1);
                                        //record.TriggerDateTime = new DateTime(triggerDateTime.Year, triggerDateTime.Month, triggerDateTime.Day, 9, 0, 0, 0, triggerDateTime.Kind);
                                        //Config File?
                                        recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.THIRDEMAIL;
                                        recordSBS.bounceBackCount += 1;
                                        recordSBS.Status = "R";
                                        recordSBS.LastStatusUpdateTime = System.DateTime.Now;

                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.SECONDEMAIL + "->" + BOUNCEBACK_STATUS.THIRDEMAIL + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.SECONDEMAIL + "->" + BOUNCEBACK_STATUS.THIRDEMAIL + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime);

                                        break;
                                    case BOUNCEBACK_STATUS.THIRDEMAIL:
                                        // 3rd Email Rebound and send 1st SMS
                                        if (!String.IsNullOrEmpty(recordSBS.MobileNo))
                                        {//MobileNo not Null
                                            recordSBS.bounceBackCount += 1;
                                            recordSBS.Status = "R";
                                            recordSBS.LastStatusUpdateTime = System.DateTime.Now;

                                            if (recordSBS.StatementType.Equals("InfoCast") || recordSBS.StatementType.Equals("CF") || recordSBS.StatementType.Equals("PB"))
                                            {
                                                recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.FIRSTSMS;

                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDEMAIL + "->" + BOUNCEBACK_STATUS.FIRSTSMS + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime));
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDEMAIL + "->" + BOUNCEBACK_STATUS.FIRSTSMS + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime);
                                            }
                                            else
                                            {
                                                recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.NonInvestSMSOTP;

                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> " + BOUNCEBACK_STATUS.NonInvestSMSOTP + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime));
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> " + BOUNCEBACK_STATUS.NonInvestSMSOTP + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime);
                                            }
                                        }
                                        else
                                        {//MobileNo Null
                                            recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.PaperStatment;
                                            recordSBS.bounceBackCount += 1;
                                            recordSBS.Status = "F";
                                            recordSBS.LastStatusUpdateTime = System.DateTime.Now;

                                            if (recordSBS.bounceBackCount == 1)
                                            {
                                                recordSBS.bounceBackStatus = "NIMM";
                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Non Invest(Missing Mobile) Case StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> NIMM" + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> F"));
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Non Invest(Missing Mobile) Case StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> NIMM" + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> F");
                                            }
                                            else
                                            {
                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invest(Missing Mobile) Case StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDEMAIL + "->" + BOUNCEBACK_STATUS.PaperStatment + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> F"));
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invest(Missing Mobile) Case StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDEMAIL + "->" + BOUNCEBACK_STATUS.PaperStatment + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> F");

                                                var statementRecord = entities.tbStatements.Where(a => a.ID.Equals(recordSBS.StatementID)).FirstOrDefault();
                                                if (statementRecord != null)
                                                {
                                                    var accountListRecord = entities.tbAccountLists.Where(b => b.ID.Equals(statementRecord.AccountID)).FirstOrDefault();
                                                    if (accountListRecord != null)
                                                    {
                                                        if (accountListRecord.RegistrationStatus.Equals("S"))
                                                        {
                                                            if (!Properties.Settings.Default.SkipChangePS.Equals("Y"))
                                                            {
                                                                accountListRecord.RegistrationStatus = "U";
                                                                accountListRecord.LastUpdate = System.DateTime.Now;
                                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbAccountLists table= " + "Registration Status: S -> U"));
                                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbAccountLists table= " + "Registration Status: S -> U");
                                                            }
                                                            else
                                                            {
                                                                //Console.WriteLine("SkipChangePS");
                                                                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip All eStatement To Paper Statement Case= StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber));
                                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip All eStatement To Paper Statement Case= StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber);
                                                            }
                                                            //Add Subscription Status Update
                                                            tbSubscriptionStatusUpdate subscriptionStatusUpdate = new tbSubscriptionStatusUpdate();
                                                            subscriptionStatusUpdate.ActionDateTime = DateTime.Now;
                                                            subscriptionStatusUpdate.RMID = accountListRecord.RMID;
                                                            subscriptionStatusUpdate.AccountType = accountListRecord.StatementType;
                                                            subscriptionStatusUpdate.AccountNumber = accountListRecord.DisplayAccountNumber;
                                                            subscriptionStatusUpdate.Action = "B";
                                                            entities.AddTotbSubscriptionStatusUpdates(subscriptionStatusUpdate);

                                                            Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbSubscriptionStatusUpdate table"));
                                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbSubscriptionStatusUpdate table");

                                                        }
                                                        else
                                                        {
                                                            Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " tbAccountLists Registration Status is U already"));
                                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " tbAccountLists Registration Status is U already");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbAccountLists table, Cannot Update Registration Status. StatementID: " + recordSBS.StatementID));
                                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbAccountLists table, Cannot Update Registration Status. StatementID: " + recordSBS.StatementID);
                                                    }
                                                }
                                                else
                                                {
                                                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbStatements table, Cannot Update Registration Status. StatementID: " + recordSBS.StatementID));
                                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbStatements table, Cannot Update Registration Status. StatementID: " + recordSBS.StatementID);
                                                }
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                //conn.Close();
                            }
                            else
                            { // 
                            }
                        }
                        else
                        {
                            if (StatementID == -1)
                            {
                                Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] No Record found in tbNotification table or Delay limit timeout, SBS Ref ID: " + SBSID + " Email Delay Limit(Hours): " + Properties.Settings.Default.emailBouncebackDelayLimit));
                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] No Record found in tbNotification table or Delay limit timeout, SBS Ref ID: " + SBSID + " Email Delay Limit(Hours): " + Properties.Settings.Default.emailBouncebackDelayLimit);
                            }
                            else if (SBSID == -1)
                            {
                                Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] No Record found in tbNotification table or Delay limit timeout, StatementID: " + StatementID + " Email Delay Limit(Hours): " + Properties.Settings.Default.emailBouncebackDelayLimit));
                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] No Record found in tbNotification table or Delay limit timeout, StatementID: " + StatementID + " Email Delay Limit(Hours): " + Properties.Settings.Default.emailBouncebackDelayLimit);
                            }
                        }

                    }
                    else if (bounceBackObj.GetType() == typeof(bounceBackSMS))
                    {
                        //System.Console.WriteLine("SMS");
                        //System.Console.WriteLine(bounceBackObj.applicationId);
                        //System.Console.WriteLine(bounceBackObj.uneReferenceNo);
                        //System.Console.WriteLine(bounceBackObj.statusOfSMS);
                        //System.Console.WriteLine(bounceBackObj.lastUpdateTimeOfSMSStatus);
                        //System.Console.WriteLine(bounceBackObj.applicationReferenceFromSourceSys);

                        //int uneReferenceNo;

                        //int statementID = Int32.Parse(substrings.GetValue(10).ToString());
                        bool isNumeric = Regex.IsMatch(bounceBackObj.uneReferenceNo, @"^\d+$");

                        //bool isNumeric = int.TryParse(bounceBackObj.uneReferenceNo, out uneReferenceNo);

                        if (!isNumeric)
                        {
                            Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Invalid UNEReferenceNo in Input File Record: " + bounceBackObj.applicationId + ", " + bounceBackObj.uneReferenceNo + ", " + bounceBackObj.statusOfSMS + ", " + bounceBackObj.lastUpdateTimeOfSMSStatus + ", " + bounceBackObj.applicationReferenceFromSourceSys));
                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Invalid UNEReferenceNo in Input File Record: " + bounceBackObj.applicationId + ", " + bounceBackObj.uneReferenceNo + ", " + bounceBackObj.statusOfSMS + ", " + bounceBackObj.lastUpdateTimeOfSMSStatus + ", " + bounceBackObj.applicationReferenceFromSourceSys);
                            continue;
                        }

                        DateTime smsDelayLimit = DateTime.Now;

                        int smsDelayLimitOffset;
                        bool isNumericSMSDelayLimitOffset = int.TryParse(Properties.Settings.Default.smsBouncebackDelayLimit, out smsDelayLimitOffset);

                        if (isNumericSMSDelayLimitOffset)
                        {
                            if (smsDelayLimitOffset >= 0)
                            {
                                smsDelayLimit = smsDelayLimit.AddHours(-smsDelayLimitOffset);
                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] SMS Delay Limit: " + smsDelayLimit + " Hours: " + smsDelayLimitOffset);
                            }
                            else
                            {
                                //Default sms delay limit 24 hours
                                smsDelayLimit = smsDelayLimit.AddHours(-24);
                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invalid SMS Delay Limit Input. Default 24 Hours: " + smsDelayLimit);
                            }
                        }
                        else
                        {
                            //Default sms delay limit 24 hours
                            smsDelayLimit = smsDelayLimit.AddHours(-24);
                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invalid SMS Delay Limit Input. Default 24 Hours: " + smsDelayLimit);
                        }


                        var recordNormal = entities.tbNotifications.Where(a => a.UNEReferenceNumber.Equals(bounceBackObj.uneReferenceNo)).Where(b => b.SentDateTime > smsDelayLimit).FirstOrDefault();

                        var recordSBS = entities.tbNotificationSBS.Where(a => a.UNEReferenceNumber.Equals(bounceBackObj.uneReferenceNo)).Where(b => b.SentDateTime > smsDelayLimit).FirstOrDefault();



                        if (recordNormal != null)
                        {
                            if (recordNormal.Status == "S")
                            {
                                //conn.Open();

                                //No PK in tbCustomerEMail, it can't use ADO
                                //It need to use SQL cmd to select
                                SqlCommand cmdSelect = new SqlCommand("SELECT TOP 1 EMailAddress, MobileNumber, AcctStatus, AcctStatusLastUpdDt, PreferredLanguage FROM tbCustomerEMail WHERE RMID = @RMID", conn);

                                cmdSelect.Parameters.AddWithValue("@RMID", recordNormal.RMID);

                                reader = cmdSelect.ExecuteReader();

                                // S is first time
                                // R is under bounceback stage

                                if (reader.HasRows)
                                {
                                    tbMISReportSM MISReportSMS = new tbMISReportSM();
                                    tbBounceBackHistory bounceBackHistorySMS = new tbBounceBackHistory();
                                    while (reader.Read())
                                    {
                                        //Add Bounce Back History (SMS)
                                        bounceBackHistorySMS.StatementID = recordNormal.StatementID;
                                        bounceBackHistorySMS.RMID = recordNormal.RMID;
                                        bounceBackHistorySMS.EmailAddress = recordNormal.EMailAddress;
                                        bounceBackHistorySMS.FailureReportDateTime = DateTime.Now;
                                        bounceBackHistorySMS.PreferredLanguage = (string)reader["PreferredLanguage"];
                                        bounceBackHistorySMS.MobileNumber = recordNormal.MobileNo;
                                        bounceBackHistorySMS.SMSSentDateTime = DateTime.ParseExact(bounceBackObj.lastUpdateTimeOfSMSStatus, "yyyyMMddHHmmss", null);
                                        bounceBackHistorySMS.SMSEmailBounceBackIndicator = "S";

                                        if (recordNormal.bounceBackCount.Equals(null) || recordNormal.bounceBackCount.Equals(0))
                                        {
                                            bounceBackHistorySMS.FirstTimeBounceBackIndicator = "Y";
                                        }
                                        else
                                        {
                                            bounceBackHistorySMS.FirstTimeBounceBackIndicator = "N";
                                        }


                                        //Add SMS MIS Report
                                        MISReportSMS.RMID = recordNormal.RMID;
                                        MISReportSMS.EmailAddress = recordNormal.EMailAddress;

                                        if (recordNormal.bounceBackCount.Equals(null))
                                        {
                                            MISReportSMS.BounceBackCount = 1;
                                        }
                                        else
                                        {
                                            MISReportSMS.BounceBackCount = recordNormal.bounceBackCount + 1;
                                        }

                                        MISReportSMS.MobileNumber = recordNormal.MobileNo;
                                        MISReportSMS.PreferredLanguage = (string)reader["PreferredLanguage"];
                                        MISReportSMS.SMSSentDateTime = recordNormal.SentDateTime;
                                        MISReportSMS.UNEReferenceNumber = recordNormal.UNEReferenceNumber;

                                    }
                                    entities.AddTotbBounceBackHistories(bounceBackHistorySMS);
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert Bounce Back History (SMS) table Success "));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert Bounce Back History (SMS) table Success  ");

                                    entities.AddTotbMISReportSMS(MISReportSMS);
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert MIS table Success "));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert MIS table Success  ");
                                }
                                else
                                {
                                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert MIS table Failed" + " Reason: Record Not Found in tbCustomerEMail"));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Insert MIS table Failed" + " Reason: Record Not Found in tbCustomerEMail");
                                }


                                if (recordNormal.bounceBackStatus.Equals(BOUNCEBACK_STATUS.FIRSTEMAIL) || recordNormal.bounceBackStatus.Equals(BOUNCEBACK_STATUS.SECONDEMAIL) || recordNormal.bounceBackStatus.Equals(BOUNCEBACK_STATUS.THIRDEMAIL))
                                {
                                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Wrong Bounce Back Status"));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Wrong Bounce Back Status");
                                    continue;
                                }

                                recordNormal.TriggerDateTime = System.DateTime.Now;

                                switch (recordNormal.bounceBackStatus.Trim())
                                {
                                    case BOUNCEBACK_STATUS.NonInvestSMSOTP:
                                        recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.NonInvestSMSOTP;
                                        recordNormal.bounceBackCount += 1;
                                        recordNormal.Status = "F";
                                        recordNormal.LastStatusUpdateTime = System.DateTime.Now;
                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Non Invest(SMSOTP) Case StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> NIS " + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> F"));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Non Invest(SMSOTP) Case StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> NIS" + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> F");
                                        break;
                                    case BOUNCEBACK_STATUS.FIRSTSMS:
                                        // 1st SMS Rebound and send 2nd SMS 
                                        recordNormal.TriggerDateTime = System.DateTime.Now.AddHours(triggerDateTimeOffset);
                                        //DateTime triggerDateTime = System.DateTime.Now.AddDays(1);
                                        //record.TriggerDateTime = new DateTime(triggerDateTime.Year, triggerDateTime.Month, triggerDateTime.Day, 9, 0, 0, 0, triggerDateTime.Kind);
                                        //Config File?
                                        recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.SECONDSMS;
                                        recordNormal.bounceBackCount += 1;
                                        recordNormal.Status = "R";
                                        recordNormal.LastStatusUpdateTime = System.DateTime.Now;

                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.FIRSTSMS + "->" + BOUNCEBACK_STATUS.SECONDSMS + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.FIRSTSMS + "->" + BOUNCEBACK_STATUS.SECONDSMS + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime);

                                        break;
                                    case BOUNCEBACK_STATUS.SECONDSMS:
                                        // 2nd SMS Rebound and send 3rd SMS
                                        recordNormal.TriggerDateTime = System.DateTime.Now.AddHours(triggerDateTimeOffset);
                                        //DateTime triggerDateTime = System.DateTime.Now.AddDays(1);
                                        //record.TriggerDateTime = new DateTime(triggerDateTime.Year, triggerDateTime.Month, triggerDateTime.Day, 9, 0, 0, 0, triggerDateTime.Kind);
                                        //Config File?
                                        recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.THIRDSMS;
                                        recordNormal.bounceBackCount += 1;
                                        recordNormal.Status = "R";
                                        recordNormal.LastStatusUpdateTime = System.DateTime.Now;

                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.SECONDSMS + "->" + BOUNCEBACK_STATUS.THIRDSMS + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.SECONDSMS + "->" + BOUNCEBACK_STATUS.THIRDSMS + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordNormal.TriggerDateTime);

                                        break;
                                    case BOUNCEBACK_STATUS.THIRDSMS:
                                        // 3rd SMS Rebound and eStatement to Paper Statement
                                        recordNormal.bounceBackStatus = BOUNCEBACK_STATUS.PaperStatment;
                                        recordNormal.bounceBackCount += 1;
                                        recordNormal.Status = "F";
                                        recordNormal.LastStatusUpdateTime = System.DateTime.Now;

                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invest / Non Invest(SMSOTP) Case StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDSMS + "->" + BOUNCEBACK_STATUS.PaperStatment + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> F"));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invest / Non Invest(SMSOTP) Case StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDSMS + "->" + BOUNCEBACK_STATUS.PaperStatment + " Bounce Back Count: " + recordNormal.bounceBackCount + " Status: " + "S -> F");

                                        var statementRecord = entities.tbStatements.Where(a => a.ID.Equals(recordNormal.StatementID)).FirstOrDefault();
                                        if (statementRecord != null)
                                        {
                                            var accountListRecord = entities.tbAccountLists.Where(b => b.ID.Equals(statementRecord.AccountID)).FirstOrDefault();
                                            if (accountListRecord != null)
                                            {
                                                if (accountListRecord.RegistrationStatus.Equals("S"))
                                                {
                                                    if (!Properties.Settings.Default.SkipChangePS.Equals("Y"))
                                                    {
                                                        accountListRecord.RegistrationStatus = "U";
                                                        accountListRecord.LastUpdate = System.DateTime.Now;
                                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbAccountLists table= " + "Registration Status: S -> U"));
                                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbAccountLists table= " + "Registration Status: S -> U");
                                                    }
                                                    else 
                                                    {
                                                        //Console.WriteLine("SkipChangePS");
                                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip All eStatement To Paper Statement Case= StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber));
                                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip All eStatement To Paper Statement Case= StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber);
                                                    }
                                                    //Add Subscription Status Update
                                                    tbSubscriptionStatusUpdate subscriptionStatusUpdate = new tbSubscriptionStatusUpdate();
                                                    subscriptionStatusUpdate.ActionDateTime = DateTime.Now;
                                                    subscriptionStatusUpdate.RMID = accountListRecord.RMID;
                                                    subscriptionStatusUpdate.AccountType = accountListRecord.StatementType;
                                                    subscriptionStatusUpdate.AccountNumber = accountListRecord.DisplayAccountNumber;
                                                    subscriptionStatusUpdate.Action = "B";
                                                    entities.AddTotbSubscriptionStatusUpdates(subscriptionStatusUpdate);

                                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbSubscriptionStatusUpdate table"));
                                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " Prepare Update tbSubscriptionStatusUpdate table");

                                                }
                                                else
                                                {
                                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal.StatementID + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " tbAccountLists Registration Status is U already"));
                                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordNormal + " UNE Reference Number: " + recordNormal.UNEReferenceNumber + " tbAccountLists Registration Status is U already");
                                                }
                                            }
                                            else
                                            {
                                                Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbAccountLists table, Cannot Update Registration Status. StatementID: " + recordNormal.StatementID));
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbAccountLists table, Cannot Update Registration Status. StatementID: " + recordNormal.StatementID);
                                            }
                                        }
                                        else
                                        {
                                            Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbStatements table, Cannot Update Registration Status. StatementID: " + recordNormal.StatementID));
                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbStatements table, Cannot Update Registration Status. StatementID: " + recordNormal.StatementID);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                //conn.Close();
                            }
                            else
                            { // Unexpected status

                            }
                        }
                        else if (recordSBS != null)
                        {
                            if (recordSBS.Status == "S")
                            {
                                //conn.Open();

                                //No PK in tbCustomerEMail, it can't use ADO
                                //It need to use SQL cmd to select
                                SqlCommand cmdSelect = new SqlCommand("SELECT TOP 1 EMailAddress, MobileNumber, AcctStatus, AcctStatusLastUpdDt, PreferredLanguage FROM tbCustomerEMail WHERE RMID = @RMID", conn);

                                cmdSelect.Parameters.AddWithValue("@RMID", recordSBS.RMID);

                                reader = cmdSelect.ExecuteReader();

                                // S is first time
                                // R is under bounceback stage

                                if (reader.HasRows)
                                {
                                    tbMISReportSM MISReportSMS = new tbMISReportSM();
                                    tbBounceBackHistory bounceBackHistorySMS = new tbBounceBackHistory();
                                    while (reader.Read())
                                    {
                                        //Add Bounce Back History (SMS)
                                        bounceBackHistorySMS.StatementID = recordSBS.StatementID;
                                        bounceBackHistorySMS.RMID = recordSBS.RMID;
                                        bounceBackHistorySMS.EmailAddress = recordSBS.EMailAddress;
                                        bounceBackHistorySMS.FailureReportDateTime = DateTime.Now;
                                        bounceBackHistorySMS.PreferredLanguage = (string)reader["PreferredLanguage"];
                                        bounceBackHistorySMS.MobileNumber = recordSBS.MobileNo;
                                        bounceBackHistorySMS.SMSSentDateTime = DateTime.ParseExact(bounceBackObj.lastUpdateTimeOfSMSStatus, "yyyyMMddHHmmss", null);
                                        bounceBackHistorySMS.SMSEmailBounceBackIndicator = "S";

                                        if (recordSBS.bounceBackCount.Equals(null) || recordSBS.bounceBackCount.Equals(0))
                                        {
                                            bounceBackHistorySMS.FirstTimeBounceBackIndicator = "Y";
                                        }
                                        else
                                        {
                                            bounceBackHistorySMS.FirstTimeBounceBackIndicator = "N";
                                        }


                                        //Add SMS MIS Report
                                        MISReportSMS.RMID = recordSBS.RMID;
                                        MISReportSMS.EmailAddress = recordSBS.EMailAddress;

                                        if (recordSBS.bounceBackCount.Equals(null))
                                        {
                                            MISReportSMS.BounceBackCount = 1;
                                        }
                                        else
                                        {
                                            MISReportSMS.BounceBackCount = recordSBS.bounceBackCount + 1;
                                        }

                                        MISReportSMS.MobileNumber = recordSBS.MobileNo;
                                        MISReportSMS.PreferredLanguage = (string)reader["PreferredLanguage"];
                                        MISReportSMS.SMSSentDateTime = recordSBS.SentDateTime;
                                        MISReportSMS.UNEReferenceNumber = recordSBS.UNEReferenceNumber;

                                    }
                                    entities.AddTotbBounceBackHistories(bounceBackHistorySMS);
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert Bounce Back History (SMS) table Success "));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert Bounce Back History (SMS) table Success  ");

                                    entities.AddTotbMISReportSMS(MISReportSMS);
                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert MIS table Success "));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert MIS table Success  ");
                                }
                                else
                                {
                                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert MIS table Failed" + " Reason: Record Not Found in tbCustomerEMail"));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Insert MIS table Failed" + " Reason: Record Not Found in tbCustomerEMail");
                                }


                                if (recordSBS.bounceBackStatus.Equals(BOUNCEBACK_STATUS.FIRSTEMAIL) || recordSBS.bounceBackStatus.Equals(BOUNCEBACK_STATUS.SECONDEMAIL) || recordSBS.bounceBackStatus.Equals(BOUNCEBACK_STATUS.THIRDEMAIL))
                                {
                                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Wrong Bounce Back Status"));
                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Wrong Bounce Back Status");
                                    continue;
                                }

                                recordSBS.TriggerDateTime = System.DateTime.Now;

                                switch (recordSBS.bounceBackStatus.Trim())
                                {
                                    case BOUNCEBACK_STATUS.NonInvestSMSOTP:
                                        recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.NonInvestSMSOTP;
                                        recordSBS.bounceBackCount += 1;
                                        recordSBS.Status = "F";
                                        recordSBS.LastStatusUpdateTime = System.DateTime.Now;
                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Non Invest(SMSOTP) Case StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> NIS " + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> F"));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Non Invest(SMSOTP) Case StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: NULL -> NIS" + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> F");
                                        break;
                                    case BOUNCEBACK_STATUS.FIRSTSMS:
                                        // 1st SMS Rebound and send 2nd SMS 
                                        recordSBS.TriggerDateTime = System.DateTime.Now.AddHours(triggerDateTimeOffset);
                                        //DateTime triggerDateTime = System.DateTime.Now.AddDays(1);
                                        //recordSBS.TriggerDateTime = new DateTime(triggerDateTime.Year, triggerDateTime.Month, triggerDateTime.Day, 9, 0, 0, 0, triggerDateTime.Kind);
                                        //Config File?
                                        recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.SECONDSMS;
                                        recordSBS.bounceBackCount += 1;
                                        recordSBS.Status = "R";
                                        recordSBS.LastStatusUpdateTime = System.DateTime.Now;

                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.FIRSTSMS + "->" + BOUNCEBACK_STATUS.SECONDSMS + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.FIRSTSMS + "->" + BOUNCEBACK_STATUS.SECONDSMS + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime);

                                        break;
                                    case BOUNCEBACK_STATUS.SECONDSMS:
                                        // 2nd SMS Rebound and send 3rd SMS
                                        recordSBS.TriggerDateTime = System.DateTime.Now.AddHours(triggerDateTimeOffset);
                                        //DateTime triggerDateTime = System.DateTime.Now.AddDays(1);
                                        //recordSBS.TriggerDateTime = new DateTime(triggerDateTime.Year, triggerDateTime.Month, triggerDateTime.Day, 9, 0, 0, 0, triggerDateTime.Kind);
                                        //Config File?
                                        recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.THIRDSMS;
                                        recordSBS.bounceBackCount += 1;
                                        recordSBS.Status = "R";
                                        recordSBS.LastStatusUpdateTime = System.DateTime.Now;

                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.SECONDSMS + "->" + BOUNCEBACK_STATUS.THIRDSMS + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.SECONDSMS + "->" + BOUNCEBACK_STATUS.THIRDSMS + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> R" + " Trigger Date Time: " + recordSBS.TriggerDateTime);

                                        break;
                                    case BOUNCEBACK_STATUS.THIRDSMS:
                                        // 3rd SMS Rebound and eStatement to Paper Statement
                                        recordSBS.bounceBackStatus = BOUNCEBACK_STATUS.PaperStatment;
                                        recordSBS.bounceBackCount += 1;
                                        recordSBS.Status = "F";
                                        recordSBS.LastStatusUpdateTime = System.DateTime.Now;

                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invest / Non Invest(SMSOTP) Case StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDSMS + "->" + BOUNCEBACK_STATUS.PaperStatment + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> F"));
                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Invest / Non Invest(SMSOTP) Case StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbNotification table= " + " Bounce Back Status: " + BOUNCEBACK_STATUS.THIRDSMS + "->" + BOUNCEBACK_STATUS.PaperStatment + " Bounce Back Count: " + recordSBS.bounceBackCount + " Status: " + "S -> F");

                                        var statementRecord = entities.tbStatements.Where(a => a.ID.Equals(recordSBS.StatementID)).FirstOrDefault();
                                        if (statementRecord != null)
                                        {
                                            var accountListRecord = entities.tbAccountLists.Where(b => b.ID.Equals(statementRecord.AccountID)).FirstOrDefault();
                                            if (accountListRecord != null)
                                            {
                                                if (accountListRecord.RegistrationStatus.Equals("S"))
                                                {
                                                    if (!Properties.Settings.Default.SkipChangePS.Equals("Y"))
                                                    {
                                                        accountListRecord.RegistrationStatus = "U";
                                                        accountListRecord.LastUpdate = System.DateTime.Now;
                                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbAccountLists table= " + "Registration Status: S -> U"));
                                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbAccountLists table= " + "Registration Status: S -> U");
                                                    }
                                                    else
                                                    {
                                                        //Console.WriteLine("SkipChangePS");
                                                        Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip All eStatement To Paper Statement Case= StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber));
                                                        Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip All eStatement To Paper Statement Case= StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber);
                                                    }
                                                    //Add Subscription Status Update
                                                    tbSubscriptionStatusUpdate subscriptionStatusUpdate = new tbSubscriptionStatusUpdate();
                                                    subscriptionStatusUpdate.ActionDateTime = DateTime.Now;
                                                    subscriptionStatusUpdate.RMID = accountListRecord.RMID;
                                                    subscriptionStatusUpdate.AccountType = accountListRecord.StatementType;
                                                    subscriptionStatusUpdate.AccountNumber = accountListRecord.DisplayAccountNumber;
                                                    subscriptionStatusUpdate.Action = "B";
                                                    entities.AddTotbSubscriptionStatusUpdates(subscriptionStatusUpdate);

                                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbSubscriptionStatusUpdate table"));
                                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " Prepare Update tbSubscriptionStatusUpdate table");

                                                }
                                                else
                                                {
                                                    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " tbAccountLists Registration Status is U already"));
                                                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] StatementID: " + recordSBS.StatementID + " UNE Reference Number: " + recordSBS.UNEReferenceNumber + " tbAccountLists Registration Status is U already");
                                                }
                                            }
                                            else
                                            {
                                                Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbAccountLists table, Cannot Update Registration Status. StatementID: " + recordSBS.StatementID));
                                                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbAccountLists table, Cannot Update Registration Status. StatementID: " + recordSBS.StatementID);
                                            }
                                        }
                                        else
                                        {
                                            Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbStatements table, Cannot Update Registration Status. StatementID: " + recordSBS.StatementID));
                                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Record found in tbStatements table, Cannot Update Registration Status. StatementID: " + recordSBS.StatementID);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                //conn.Close();
                            }
                            else
                            { // Unexpected status

                            }
                        }
                        else 
                        {
                            Util.Log(logType.Warning, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] No Record found in tbNotification table or Delay limit timeout, uneReferenceNo: " + bounceBackObj.uneReferenceNo + " SMS Delay Limit(Hours): " + Properties.Settings.Default.smsBouncebackDelayLimit));
                            Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Warning] No Record found in tbNotification table or Delay limit timeout, uneReferenceNo: " + bounceBackObj.uneReferenceNo + " SMS Delay Limit(Hours): " + Properties.Settings.Default.smsBouncebackDelayLimit);
                        }
                    }
                }
                //if(entities.SaveChanges() > 0)
                //{
                //    Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Database Save Change Success"));
                //    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Database Save Change Success");    
                //}
                //else
                //{
                //    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Database Save Change Failed"));
                //    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Database Save Change Failed"); 
                //}

                try
                {
                    entities.SaveChanges();
                    if (conn != null && (conn.State & ConnectionState.Open) != 0) conn.Close();
                }
                catch (Exception ex)
                {
                    String result = ex.Message.ToString();
                    Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Database Save Change Error: " + result));
                    Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Database Save Change Error: " + result);
                }

                Util.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program bounceBackNoticeApp End"));
                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program bounceBackNoticeApp End");
                System.Threading.Thread.Sleep(5000);
                return;
            }
            catch (Exception ex)
            {
                String result = ex.Message.ToString();
                Util.Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Unexpected Error: " + result));
                Util.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Unexpected Error: " + result);
            }

        }

        #region "Common Tools"
        private static String trimming(String str)
        {
            if (String.IsNullOrEmpty(str))
                return str;
            else
                return str.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
        }

        #endregion

        static class BOUNCEBACK_STATUS
        {
            public const string FIRSTEMAIL = "E1";
            public const string SECONDEMAIL = "E2";
            public const string THIRDEMAIL = "E3";
            public const string FIRSTSMS = "S1";
            public const string SECONDSMS = "S2";
            public const string THIRDSMS = "S3";
            public const string PaperStatment = "PS";
            public const string NonInvestSMSOTP = "NIS";
        }

        interface bounceBackInterface
        {
            string deliveredDate
            {
                get;
                set;
            }

            string subject
            {
                get;
                set;
            }

            string sendTo
            {
                get;
                set;
            }

            string failureReason
            {
                get;
                set;
            }

            string messageId
            {
                get;
                set;
            }

            string SMTPEnvid
            {
                get;
                set;
            }

            string status
            {
                get;
                set;
            }

            string applicationId
            {
                get;
                set;
            }

            string uneReferenceNo
            {
                get;
                set;
            }

            string statusOfSMS
            {
                get;
                set;
            }

            string lastUpdateTimeOfSMSStatus
            {
                get;
                set;
            }

            string applicationReferenceFromSourceSys
            {
                get;
                set;
            }

        }

        class bounceBackEmail : bounceBackInterface
        {
            public string deliveredDate
            {
                get;
                set;
            }

            public string subject
            {
                get;
                set;
            }

            public string sendTo
            {
                get;
                set;
            }

            public string failureReason
            {
                get;
                set;
            }

            public string messageId
            {
                get;
                set;
            }

            public string SMTPEnvid
            {
                get;
                set;
            }

            public string status
            {
                get;
                set;
            }

            public string applicationId
            {
                get;
                set;
            }

            public string uneReferenceNo
            {
                get;
                set;
            }

            public string statusOfSMS
            {
                get;
                set;
            }

            public string lastUpdateTimeOfSMSStatus
            {
                get;
                set;
            }

            public string applicationReferenceFromSourceSys
            {
                get;
                set;
            }


            public bounceBackEmail(String[] substrings)
            {
                try
                {
                    deliveredDate = substrings.GetValue(1).ToString();
                    subject = substrings.GetValue(3).ToString();
                    sendTo = substrings.GetValue(5).ToString();
                    failureReason = substrings.GetValue(7).ToString();
                    messageId = substrings.GetValue(9).ToString();
                    SMTPEnvid = substrings.GetValue(11).ToString();
                    status = substrings.GetValue(13).ToString();
                }
                catch (Exception ex)
                {
                    String result = ex.Message.ToString();
                    Console.WriteLine(result);
                }

            }
        }

        class bounceBackSMS : bounceBackInterface
        {
            public string deliveredDate
            {
                get;
                set;
            }

            public string subject
            {
                get;
                set;
            }

            public string sendTo
            {
                get;
                set;
            }

            public string failureReason
            {
                get;
                set;
            }

            public string messageId
            {
                get;
                set;
            }

            public string SMTPEnvid
            {
                get;
                set;
            }

            public string status
            {
                get;
                set;
            }


            public string applicationId
            {
                get;
                set;
            }

            public string uneReferenceNo
            {
                get;
                set;
            }

            public string statusOfSMS
            {
                get;
                set;
            }

            public string lastUpdateTimeOfSMSStatus
            {
                get;
                set;
            }

            public string applicationReferenceFromSourceSys
            {
                get;
                set;
            }

            public bounceBackSMS(String[] substrings)
            {
                try
                {
                    applicationId = substrings.GetValue(0).ToString();
                    uneReferenceNo = substrings.GetValue(1).ToString();
                    statusOfSMS = substrings.GetValue(2).ToString();
                    lastUpdateTimeOfSMSStatus = substrings.GetValue(3).ToString();
                    applicationReferenceFromSourceSys = substrings.GetValue(4).ToString();
                }
                catch (Exception ex)
                {
                    String result = ex.Message.ToString();
                    Console.WriteLine(result);
                }
            }

        }

    }

}
