﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace bounceBackNoticeApp
{
    class Util
    {
        static EstatementEntities Entity = new EstatementEntities();
        //static EStatementEntities Entity = new EStatementEntities();
        static String logFilename = String.Format("{0}_{1:yyyy-MM-dd}.log", Properties.Settings.Default.AppName, DateTime.Now);
        static String logPath = Path.Combine(Properties.Settings.Default.LogPath, logFilename);

        public static void Log(logType type, String content)
        {
            try
            {
                DateTime now = DateTime.Now;
                tbLog log = new tbLog();
                log.Type = Enum.GetName(typeof(logType), type);
                if (content.Length < 900)
                {
                    log.Content = content;
                }
                else
                {
                    log.Content = content.Substring(0, 900);
                    log.Content = "Content Out of Database Input Range, Content truncated to 900 characters for reference: " + log.Content;
                }
                log.CreateBy = Properties.Settings.Default.AppName;
                log.ModifyBy = Properties.Settings.Default.AppName;
                log.CreateDate = now;
                log.LastUpdate = now;
                Entity.AddTotbLogs(log);
                Entity.SaveChanges();

                if (type != logType.Warning)
                {
                    Console.WriteLine(content);
                }
            }
            catch (Exception e) 
            {
                Console.WriteLine("Log Insert Database Error. " + "Reason: " + e.Message.ToString());             
            }
        }

        public static void writeLog(String content)
        {
            try
            {
                if (File.Exists(logPath))
                {
                    if (!File.GetLastWriteTime(logPath).ToString("yyyyMMdd").Equals(DateTime.Now.ToString("yyyyMMdd")))
                    {
                        String newLogPath = String.Format("{0}_{1:yyyyMMdd}.log", logPath.Replace(".log", ""), File.GetLastWriteTime(logPath).ToString("yyyyMMdd"));
                        File.Delete(newLogPath);
                        File.Move(logPath, newLogPath);
                    }
                }
                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    sw.Close();
                }

                //Log(logType.Info, content);
            }
            catch (Exception e) 
            {
                Console.WriteLine("Write Log File Error. " + "Reason: " + e.Message.ToString());  
            }
        }
    }
}
