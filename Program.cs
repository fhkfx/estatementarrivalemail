﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using EStatementArrivalEmail.UNE_WS;
using System.Web.Script;
using System.IO;

namespace EStatementArrivalEmail
{
    public enum logType { Info, Error, Warning };


    class Program
    {
        static String logFilename = String.Format("{0}_{1:yyyy-MM-dd}.log", Properties.Settings.Default.AppName, DateTime.Now);
        static String logPath = Path.Combine(Properties.Settings.Default.LogPath, logFilename);

        static void Main(string[] args)
        {
            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["DBConnectString"]);
            //There is already an open DataReader associated with this Command which must be closed first  
            //MultipleActiveResultSets=true and change view 
            SqlDataReader reader = null;
            SqlDataReader readerProductInfo = null;

            //start testing
            //Console.WriteLine("Send Email status: " + test().status);
            //Console.WriteLine("testJSON: " + testJSON());
            //End Testing

            if (args.Length != 0 && args[0] == "mobileNo")
            {
                if (args.Length != 4)
                {
                    Console.WriteLine("Incorrect no. of args. Command format should be EStatementArrivalEmail mobileNo <mobileNo(Int)> <templateID> <eStatementID(Int)> ");
                    return;
                }
                String mobileNo = args[1];
                String appID = "ES";
                String templateID = args[2];
                String appCountryCode = "HK";
                int eStatementID = 0;
                if (!int.TryParse(args[3], out eStatementID))
                {
                    Console.WriteLine("Command format error <eStatementID>: " + args[3] + " is not integer.");
                    Console.WriteLine("Command format should be EStatementArrivalEmail mobileNo <mobileNo(Int)> <templateID> <eStatementID(Int)> ");
                    return;
                }
                Console.WriteLine("Send SMS to: " + mobileNo);
                //NotificationEmail(int refNo, String category, String engDesc, String tradChiDesc, String displayAccountNumber)
                NotificationEmail email =
                        new NotificationEmail("", eStatementID.ToString(), "Individual", "Monthly Statement", "月結單", "ABC-1122334455");
                conn.Open();
                UNE_WS.SendMessageResponse result = email.SendByUNE(appID, templateID, appCountryCode, "", mobileNo, eStatementID, "", "", conn);
                conn.Close();
                Console.WriteLine("UNE Result returnCode: " + result.returnCode + " returnMsg: " + result.returnMsg + " refNo: " + result.refNo + " status: " + result.status);
                return;
            }


            try
            {
                conn.Open();
                Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program EStatementArrivalEmail Start"), conn);
                writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program EStatementArrivalEmail Start");

                //Normal Case Stored Procedure
                SqlCommand cmd = new SqlCommand("GenerateNotificationList", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();

                //SuperBank Case Stored Procedure
                cmd = new SqlCommand("GenerateNotificationSBSList", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();

                //Normal Case Update SQL
                SqlCommand cmdUpdate = new SqlCommand("UPDATE tbNotification SET DisplayAccountNumber = @DisplayAccountNumber, SentDateTime = @SentDateTime, Status = @Status, EMailReference = @EMailReference, EMailSubject = @EMailSubject, EMailContent = @EMailContent, ErrorDetail = @ErrorDetail, LastStatusUpdateTime = @LastStatusUpdateTime, UNEReferenceNumber = @UNEReferenceNumber WHERE StatementID = @StatementID", conn);
                SqlParameterCollection parameters = cmdUpdate.Parameters;

                //SuperBank Case Update SQL
                SqlCommand cmdUpdateSBS = new SqlCommand("UPDATE tbNotificationSBS SET DisplayAccountNumber = @DisplayAccountNumber, SentDateTime = @SentDateTime, Status = @Status, EMailReference = @EMailReference, EMailSubject = @EMailSubject, EMailContent = @EMailContent, ErrorDetail = @ErrorDetail, LastStatusUpdateTime = @LastStatusUpdateTime, UNEReferenceNumber = @UNEReferenceNumber WHERE StatementID = @StatementID AND RMID = @RMID AND DisplayAccountNumber = @DisplayAccountNumber", conn);
                SqlParameterCollection parametersSBS = cmdUpdateSBS.Parameters;

                //Normal Case Parameters Prepare
                SqlParameter paramDisplayAccountNumber = parameters.Add("@DisplayAccountNumber", SqlDbType.NVarChar, 100);
                SqlParameter paramSentDateTime = parameters.Add("@SentDateTime", SqlDbType.DateTime);
                SqlParameter paramStatus = parameters.Add("@Status", SqlDbType.Char, 1);
                SqlParameter paramEMailRef = parameters.Add("@EMailReference", SqlDbType.Char, 36);
                SqlParameter paramEMailSubject = parameters.Add("@EMailSubject", SqlDbType.NVarChar);
                SqlParameter paramEMailContent = parameters.Add("@EMailContent", SqlDbType.NVarChar);
                SqlParameter paramErrorDetail = parameters.Add("@ErrorDetail", SqlDbType.NVarChar);
                SqlParameter paramStatementID = parameters.Add("@StatementID", SqlDbType.Int);
                SqlParameter paramLastStatusUpdateTime = parameters.Add("@LastStatusUpdateTime", SqlDbType.DateTime);
                SqlParameter paramUNEReferenceNumber = parameters.Add("@UNEReferenceNumber", SqlDbType.NVarChar);

                paramUNEReferenceNumber.Value = DBNull.Value;
                //paramStatus.Value = DBNull.Value;
                //paramEMailRef.Value = DBNull.Value;
                //paramEMailSubject.Value = DBNull.Value;
                //paramEMailContent.Value = DBNull.Value;
                //paramErrorDetail.Value = DBNull.Value;
                //paramStatementID.Value = DBNull.Value;

                //SuperBank Case Parameters Prepare
                SqlParameter paramRMIDSBS = parametersSBS.Add("@RMID", SqlDbType.NVarChar, 100);
                SqlParameter paramDisplayAccountNumberSBS = parametersSBS.Add("@DisplayAccountNumber", SqlDbType.NVarChar, 100);
                SqlParameter paramSentDateTimeSBS = parametersSBS.Add("@SentDateTime", SqlDbType.DateTime);
                SqlParameter paramStatusSBS = parametersSBS.Add("@Status", SqlDbType.Char, 1);
                SqlParameter paramEMailRefSBS = parametersSBS.Add("@EMailReference", SqlDbType.Char, 36);
                SqlParameter paramEMailSubjectSBS = parametersSBS.Add("@EMailSubject", SqlDbType.NVarChar);
                SqlParameter paramEMailContentSBS = parametersSBS.Add("@EMailContent", SqlDbType.NVarChar);
                SqlParameter paramErrorDetailSBS = parametersSBS.Add("@ErrorDetail", SqlDbType.NVarChar);
                SqlParameter paramStatementIDSBS = parametersSBS.Add("@StatementID", SqlDbType.Int);
                SqlParameter paramLastStatusUpdateTimeSBS = parametersSBS.Add("@LastStatusUpdateTime", SqlDbType.DateTime);
                SqlParameter paramUNEReferenceNumberSBS = parametersSBS.Add("@UNEReferenceNumber", SqlDbType.NVarChar);

                paramUNEReferenceNumberSBS.Value = DBNull.Value;
                //paramStatusSBS.Value = DBNull.Value;
                //paramEMailRefSBS.Value = DBNull.Value;
                //paramEMailSubjectSBS.Value = DBNull.Value;
                //paramEMailContentSBS.Value = DBNull.Value;
                //paramErrorDetailSBS.Value = DBNull.Value;
                //paramStatementIDSBS.Value = DBNull.Value;
                

                //20160629 modify
                //SqlCommand cmdSelect = new SqlCommand("SELECT TOP 1 StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, EngDesc, TradChiDesc FROM vwPendingNotification", conn);
                SqlCommand cmdSelectP = new SqlCommand("SELECT TOP 1 ID, StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, EngDesc, TradChiDesc, statementFreq, Advice_Title, Product_Name, Product_Type FROM vwPendingNotification", conn);
                reader = cmdSelectP.ExecuteReader();
                
                while (reader.Read())
                {
                    Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwPendingNotification Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"]), conn);
                    writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwPendingNotification Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"]);

                    paramDisplayAccountNumber.Value = (String)reader["DisplayAccountNumber"];

                    if (String.IsNullOrEmpty((String)reader["DisplayAccountNumber"]))
                    {
                        if (((String)reader["Category"]).Equals("FixDeposit")) 
                        {
                            SqlCommand cmdSelectProductInfo = new SqlCommand("SELECT TOP 1 productInfo FROM tbStatement WHERE ID = " + (int)reader["StatementID"], conn);
                            readerProductInfo = cmdSelectProductInfo.ExecuteReader();
                            while (readerProductInfo.Read())
                            {
                                paramDisplayAccountNumber.Value = ((String)readerProductInfo["productInfo"]).Trim();
                            }
                            readerProductInfo.Close();
                        }
                    }

                    NotificationEmail email;

                    if ((((String)reader["Category"]).Equals("SBS") || ((String)reader["Category"]).Equals("AVQ")))
                    {
                        if (((String)reader["statementFreq"]).Equals("A"))
                        {
                            email = new NotificationEmail(((int)reader["ID"]).ToString(), ((int)reader["StatementID"]).ToString(), (String)reader["Category"], (String)reader["EngDesc"], (String)reader["TradChiDesc"], paramDisplayAccountNumber.Value.ToString().Trim(), reader["Advice_Title"] == System.DBNull.Value ? null : ((String)reader["Advice_Title"]), reader["Product_Name"] == System.DBNull.Value ? null : ((String)reader["Product_Name"]), reader["Product_Type"] == System.DBNull.Value ? null : ((String)reader["Product_Type"]));
                            //
                        }
                        else 
                        {
                            email = new NotificationEmail(((int)reader["ID"]).ToString(), ((int)reader["StatementID"]).ToString(), (String)reader["Category"], (String)reader["EngDesc"], (String)reader["TradChiDesc"], paramDisplayAccountNumber.Value.ToString().Trim());
                        }
                        
                    }
                    else 
                    {
                        email = new NotificationEmail("", ((int)reader["StatementID"]).ToString(), (String)reader["Category"], (String)reader["EngDesc"], (String)reader["TradChiDesc"], paramDisplayAccountNumber.Value.ToString().Trim());
                    }


                    paramStatementID.Value = (int)reader["StatementID"];
                    //20160523 comment
                    //paramEMailSubject.Value = email.Subject;
                    //paramEMailContent.Value = email.Content;
                    paramEMailSubject.Value = DBNull.Value;
                    paramEMailContent.Value = DBNull.Value;
                    paramEMailRef.Value = email.EMailRef;

                    if (reader["EMailAddress"] is DBNull)
                    {
                        Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Email Address " + "eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"]), conn);
                        writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Email Address " + "eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"]);

                        paramStatus.Value = "F";
                        paramErrorDetail.Value = "No Email Address";

                    }
                    else
                    {
                        try
                        {
                            String templateID = Properties.Settings.Default.EMailTemplateIDForP;
                            if (((String)reader["Category"]).Equals("SBS"))
                            {
                                templateID = Properties.Settings.Default.EMailTemplateIDSBSForP;
                            }
                            
                            if (((String)reader["Category"]).Equals("AVQ"))
                            {
                                templateID = Properties.Settings.Default.EMailTemplateIDAVQForP;
                            }

                            //20160629 modify
                            /*
                            String sFreq = (String)reader["StatementType"];
                            if(String.Compare(sFreq,"InfoCast-EA") == 0){
                                templateID = "ESADVIC001";
                            }*/
                            String sFreq = (String)reader["statementFreq"];
                            if (String.Compare(sFreq, "A") == 0)
                            {
                                templateID = Properties.Settings.Default.EMailTemplateIDForAdvice;
                                if (((String)reader["Category"]).Equals("SBS"))
                                {
                                    templateID = Properties.Settings.Default.EMailTemplateIDSBSForAdvice;
                                }

                                if (((String)reader["Category"]).Equals("AVQ"))
                                {
                                    templateID = Properties.Settings.Default.EMailTemplateIDAVQForAdvice;
                                }
                            }
                            System.Threading.Thread.Sleep(Int32.Parse(Properties.Settings.Default.EMailTriggerDelayTimeForP));
                            UNE_WS.SendMessageResponse response = email.SendByUNE("ES", templateID, "HK", (String)reader["EMailAddress"], "", (int)reader["StatementID"], "", "", conn);
                            Console.WriteLine("Start response.status: " + response.status + " reponse.returnMsg: " + response.returnMsg);

                            Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwPendingNotification Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"]), conn);
                            writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwPendingNotification Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"]);


                            if (String.Compare(response.status.ToString(), "A") == 0)
                            {
                                Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwPendingNotification Success Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " UNEReferenceNumber: " + response.refNo), conn);
                                writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwPendingNotification Success Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " UNEReferenceNumber: " + response.refNo);
                                paramUNEReferenceNumber.Value = response.refNo;
                                paramStatus.Value = "S";
                                paramErrorDetail.Value = DBNull.Value;
                            }
                            else
                            {
                                if (response.refNo != null)
                                {
                                    paramUNEReferenceNumber.Value = response.refNo;
                                }
                                else 
                                {
                                    response.refNo = "";
                                }
                                paramStatus.Value = "F";
                                if (response.returnMsg != null)
                                {
                                    Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] vwPendingNotification Failed Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " UNEReferenceNumber: " + response.refNo + " Reason: " + response.returnMsg.ToString()), conn);
                                    writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] vwPendingNotification Failed Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " UNEReferenceNumber: " + response.refNo + " Reason: " + response.returnMsg.ToString());

                                    paramErrorDetail.Value = response.returnMsg.ToString();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            String result = ex.Message.ToString();

                            Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Unexpected Error: " + result), conn);
                            writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Unexpected Error: " + result);

                            paramStatus.Value = "F";
                            paramErrorDetail.Value = ex.ToString();
                        }
                    }

                    
                    paramSentDateTime.Value = DateTime.Now;
                    paramLastStatusUpdateTime.Value = DateTime.Now;
                    //20160523 debug
                    Console.WriteLine("eStatementID:" + paramStatementID.Value + " paramErrorDetail:" + paramErrorDetail);


                    //SuperBank Case Checking
                    //If Yes, Update tbNotificationSBS
                    if (((String)reader["Category"]).Equals("SBS") || ((String)reader["Category"]).Equals("AVQ"))
                    {
                        paramDisplayAccountNumberSBS.Value = paramDisplayAccountNumber.Value;
                        paramStatementIDSBS.Value = paramStatementID.Value;
                        paramEMailSubjectSBS.Value = paramEMailSubject.Value;
                        paramEMailContentSBS.Value = paramEMailContent.Value;
                        paramEMailRefSBS.Value = paramEMailRef.Value;
                        paramUNEReferenceNumberSBS.Value = paramUNEReferenceNumber.Value;
                        paramStatusSBS.Value = paramStatus.Value;
                        paramErrorDetailSBS.Value = paramErrorDetail.Value;
                        paramSentDateTimeSBS.Value = DateTime.Now;
                        paramLastStatusUpdateTimeSBS.Value = DateTime.Now;
                        paramRMIDSBS.Value = (String)reader["RMID"];

                        cmdUpdateSBS.ExecuteNonQuery();
                    }
                    else //Else, Update tbNotification
                    {
                        cmdUpdate.ExecuteNonQuery();
                    }
                    reader.Close();
                    reader = cmdSelectP.ExecuteReader();
                }


                SqlCommand cmdSelectR = new SqlCommand("SELECT TOP 1 ID, StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, EngDesc, TradChiDesc, statementFreq, bounceBackStatus, MobileNo, PreferredLanguage, Advice_Title, Product_Name, Product_Type FROM vwReboundNotification", conn);
                reader = cmdSelectR.ExecuteReader();


                while (reader.Read())
                {
                    String bounceBackStatus = reader["bounceBackStatus"].ToString();
                    switch (bounceBackStatus.Trim())
                    {
                        case BOUNCEBACK_STATUS.FIRSTEMAIL:
                        case BOUNCEBACK_STATUS.SECONDEMAIL:
                        case BOUNCEBACK_STATUS.THIRDEMAIL:
                            {

                                Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification EMail Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus), conn);
                                writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification EMail Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus);

                                NotificationEmail email;

                                if ((((String)reader["Category"]).Equals("SBS") || ((String)reader["Category"]).Equals("AVQ")))
                                {

                                    if (((String)reader["statementFreq"]).Equals("A"))
                                    {
                                        email = new NotificationEmail(((int)reader["ID"]).ToString(), ((int)reader["StatementID"]).ToString(), (String)reader["Category"], (String)reader["EngDesc"], (String)reader["TradChiDesc"], (String)reader["DisplayAccountNumber"], reader["Advice_Title"] == System.DBNull.Value ? null : ((String)reader["Advice_Title"]), reader["Product_Name"] == System.DBNull.Value ? null : ((String)reader["Product_Name"]), reader["Product_Type"] == System.DBNull.Value ? null : ((String)reader["Product_Type"]));
                                        //NotificationEmail(int refNo, String category, String engDesc, String tradChiDesc, String displayAccountNumber, String Advice_Title, String Product_Name, String Product_Type)
                                    }
                                    else 
                                    {
                                        email = new NotificationEmail(((int)reader["ID"]).ToString(), ((int)reader["StatementID"]).ToString(), (String)reader["Category"], (String)reader["EngDesc"], (String)reader["TradChiDesc"], (String)reader["DisplayAccountNumber"]);
                                    }
                                }
                                else
                                {

                                    email = new NotificationEmail("", ((int)reader["StatementID"]).ToString(), (String)reader["Category"], (String)reader["EngDesc"], (String)reader["TradChiDesc"], (String)reader["DisplayAccountNumber"]);
                                }

                                if (reader["EMailAddress"] is DBNull)
                                {
                                    Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Email Address " + "eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus), conn);
                                    writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Email Address " + "eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus);

                                    paramStatus.Value = "F";
                                    paramErrorDetail.Value = "No Email Address";
                                    break;
                                }
                                else
                                {
                                    Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification EMail Case= " + "EMail Address: " + (string)reader["EMailAddress"]), conn);
                                    writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification EMail Case= " + "EMail Address: " + (string)reader["EMailAddress"]);
                                }

                                try
                                {

                                    paramStatementID.Value = (int)reader["StatementID"];
                                    paramEMailSubject.Value = DBNull.Value;
                                    paramEMailContent.Value = DBNull.Value;
                                    paramEMailRef.Value = email.EMailRef;

                                    String templateID = Properties.Settings.Default.EMailTemplateIDForR;
                                    if (((String)reader["Category"]).Equals("SBS"))
                                    {
                                        templateID = Properties.Settings.Default.EMailTemplateIDSBSForR;
                                    }

                                    if (((String)reader["Category"]).Equals("AVQ"))
                                    {
                                        templateID = Properties.Settings.Default.EMailTemplateIDAVQForR;
                                    }

                                    String sFreq = (String)reader["statementFreq"];
                                    if (String.Compare(sFreq, "A") == 0)
                                    {
                                        templateID = Properties.Settings.Default.EMailTemplateIDForAdvice;
                                        if (((String)reader["Category"]).Equals("SBS"))
                                        {
                                            templateID = Properties.Settings.Default.EMailTemplateIDSBSForAdvice;
                                        }

                                        if (((String)reader["Category"]).Equals("AVQ"))
                                        {
                                            templateID = Properties.Settings.Default.EMailTemplateIDAVQForAdvice;
                                        }
                                    }
                                    System.Threading.Thread.Sleep(Int32.Parse(Properties.Settings.Default.EMailTriggerDelayTimeForR));
                                    UNE_WS.SendMessageResponse response = email.SendByUNE("ES", templateID, "HK", (String)reader["EMailAddress"], "", (int)reader["StatementID"], (String)reader["PreferredLanguage"], "R", conn);
                                    Console.WriteLine("Start response.status: " + response.status + " reponse.returnMsg:" + response.returnMsg);

                                    if (String.Compare(response.status.ToString(), "A") == 0)
                                    {
                                        Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification EMail Success Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus + " UNEReferenceNumber: " + response.refNo), conn);
                                        writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification EMail Success Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus + " UNEReferenceNumber: " + response.refNo);
                                        paramUNEReferenceNumber.Value = response.refNo;
                                        paramStatus.Value = "S";
                                        paramErrorDetail.Value = DBNull.Value;
                                    }
                                    else
                                    {
                                        if (response.refNo != null)
                                        {
                                            paramUNEReferenceNumber.Value = response.refNo;
                                        }
                                        else
                                        {
                                            response.refNo = "";
                                        }
                                        paramStatus.Value = "F";
                                        if (response.returnMsg != null)
                                        {
                                            Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] vwReboundNotification EMail Failed Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus + " UNEReferenceNumbe: " + response.refNo + " Reason: " + response.returnMsg.ToString()), conn);
                                            writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] vwReboundNotification EMail Failed Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus + " UNEReferenceNumbe: " + response.refNo + " Reason: " + response.returnMsg.ToString());

                                            paramErrorDetail.Value = response.returnMsg.ToString();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    String result = ex.Message.ToString();
                                    Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Unexpected Error: " + result), conn);
                                    writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Unexpected Error: " + result);

                                    paramStatus.Value = "F";
                                    paramErrorDetail.Value = ex.ToString();
                                    break;
                                }
                                break;
                            }
                        case BOUNCEBACK_STATUS.FIRSTSMS:
                        case BOUNCEBACK_STATUS.SECONDSMS:
                        case BOUNCEBACK_STATUS.THIRDSMS:
                        case BOUNCEBACK_STATUS.NonInvestSMSOTP:
                            {
                                Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification SMS Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus), conn);
                                writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification SMS Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus);

                                NotificationEmail SMS;

                                if ((((String)reader["Category"]).Equals("SBS") || ((String)reader["Category"]).Equals("AVQ")))
                                {
                                    if (((String)reader["statementFreq"]).Equals("A"))
                                    {
                                        SMS = new NotificationEmail(((int)reader["ID"]).ToString(), ((int)reader["StatementID"]).ToString(), (String)reader["Category"], (String)reader["EngDesc"], (String)reader["TradChiDesc"], (String)reader["DisplayAccountNumber"], reader["Advice_Title"] == System.DBNull.Value ? null : ((String)reader["Advice_Title"]), reader["Product_Name"] == System.DBNull.Value ? null : ((String)reader["Product_Name"]), reader["Product_Type"] == System.DBNull.Value ? null : ((String)reader["Product_Type"]));
                                        //NotificationEmail(int refNo, String category, String engDesc, String tradChiDesc, String displayAccountNumber, String Advice_Title, String Product_Name, String Product_Type)
                                    }
                                    else 
                                    {
                                        SMS = new NotificationEmail(((int)reader["ID"]).ToString(), ((int)reader["StatementID"]).ToString(), (String)reader["Category"], (String)reader["EngDesc"], (String)reader["TradChiDesc"], (String)reader["DisplayAccountNumber"]);
                                    }
                                
                                }
                                else
                                {
                                    SMS = new NotificationEmail("", ((int)reader["StatementID"]).ToString(), (String)reader["Category"], (String)reader["EngDesc"], (String)reader["TradChiDesc"], (String)reader["DisplayAccountNumber"]);
                                }

                                if (reader["MobileNo"] is DBNull)
                                {
                                    Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Mobile Number " + "eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus), conn);
                                    writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] No Mobile Number " + "eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus);

                                    paramStatus.Value = "F";
                                    paramErrorDetail.Value = "No Mobile Number";
                                    break;
                                }
                                else
                                {
                                    Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification SMS Case= " + "Mobile Number: " + (string)reader["MobileNo"]), conn);
                                    writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification SMS Case= " + "Mobile Number: " + (string)reader["MobileNo"]);

                                }

                                try
                                {
                                    paramStatementID.Value = (int)reader["StatementID"];
                                    paramEMailSubject.Value = DBNull.Value;
                                    paramEMailContent.Value = DBNull.Value;
                                    paramEMailRef.Value = SMS.EMailRef;

                                    //String templateID = "TEST_SMS";
                                    String templateID = Properties.Settings.Default.SMSTemplateID;
                                    if (((String)reader["Category"]).Equals("SBS"))
                                    {
                                        templateID = Properties.Settings.Default.SMSTemplateIDSBS;
                                    }

                                    if (((String)reader["Category"]).Equals("AVQ"))
                                    {
                                        templateID = Properties.Settings.Default.SMSTemplateIDAVQ;
                                    }
                                    System.Threading.Thread.Sleep(Int32.Parse(Properties.Settings.Default.SMSTriggerDelayTimeForR));
                                    UNE_WS.SendMessageResponse response = SMS.SendByUNE("ES", templateID, "HK", "", (String)reader["MobileNo"], (int)reader["StatementID"], (String)reader["PreferredLanguage"], "R", conn);
                                    Console.WriteLine("Start response.status: " + response.status + " reponse.returnMsg:" + response.returnMsg);


                                    if (String.Compare(response.status.ToString(), "A") == 0)
                                    {
                                        Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification SMS Success Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus + " UNEReferenceNumber: " + response.refNo), conn);
                                        writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] vwReboundNotification SMS Success Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus + " UNEReferenceNumber: " + response.refNo);
                                        paramUNEReferenceNumber.Value = response.refNo;
                                        paramStatus.Value = "S";
                                        paramErrorDetail.Value = DBNull.Value;
                                    }
                                    else
                                    {
                                        if (response.refNo != null)
                                        {
                                            paramUNEReferenceNumber.Value = response.refNo;
                                        }
                                        else 
                                        {
                                            response.refNo = "";
                                        }
                                        paramStatus.Value = "F";
                                        if (response.returnMsg != null)
                                        {
                                            Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] vwReboundNotification SMS Failed Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus + " UNEReferenceNumber: " + response.refNo + " Reason: " + response.returnMsg.ToString()), conn);
                                            writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] vwReboundNotification SMS Failed Case= " + "Start eStatementID:" + (int)reader["StatementID"] + " " + (String)reader["Category"] + " " + (String)reader["EngDesc"] + " " + (String)reader["TradChiDesc"] + " " + (String)reader["DisplayAccountNumber"] + " " + bounceBackStatus + " UNEReferenceNumber: " + response.refNo + " Reason: " + response.returnMsg.ToString());

                                            paramErrorDetail.Value = response.returnMsg.ToString();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    String result = ex.Message.ToString();

                                    Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Unexpected Error: " + result), conn);
                                    writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Unexpected Error: " + result);

                                    paramStatus.Value = "F";
                                    paramErrorDetail.Value = ex.ToString();
                                    break;
                                }

                                break;
                            }
                        case BOUNCEBACK_STATUS.PapaerStatment:
                            break;
                        default:
                            break;
                    }
                    paramDisplayAccountNumber.Value = (String)reader["DisplayAccountNumber"];
                    
                    paramSentDateTime.Value = DateTime.Now;
                    paramLastStatusUpdateTime.Value = DateTime.Now;
                    //Console.WriteLine("eStatementID:" + paramStatementID.Value + " paramErrorDetail:" + paramErrorDetail);

                    //SuperBank Case Checking
                    //If Yes, Update tbNotificationSBS
                    if (((String)reader["Category"]).Equals("SBS") || ((String)reader["Category"]).Equals("AVQ"))
                    {
                        paramDisplayAccountNumberSBS.Value = paramDisplayAccountNumber.Value;
                        paramStatementIDSBS.Value = paramStatementID.Value;
                        paramEMailSubjectSBS.Value = paramEMailSubject.Value;
                        paramEMailContentSBS.Value = paramEMailContent.Value;
                        paramEMailRefSBS.Value = paramEMailRef.Value;
                        paramUNEReferenceNumberSBS.Value = paramUNEReferenceNumber.Value;
                        paramStatusSBS.Value = paramStatus.Value;
                        paramErrorDetailSBS.Value = paramErrorDetail.Value;
                        paramSentDateTimeSBS.Value = DateTime.Now;
                        paramLastStatusUpdateTimeSBS.Value = DateTime.Now;
                        paramRMIDSBS.Value = (String)reader["RMID"];

                        cmdUpdateSBS.ExecuteNonQuery();
                    }
                    else //Else, Update tbNotification
                    {
                        cmdUpdate.ExecuteNonQuery();
                    }
                    
                    reader.Close();
                    reader = cmdSelectR.ExecuteReader();
                }

            }
            catch (Exception ex)
            {
                String result = ex.Message.ToString();
                Log(logType.Error, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Unexpected Error: " + result), conn);
                writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Error] Unexpected Error: " + result);
            }
            finally
            {
                Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program EStatementArrivalEmail End"), conn);
                writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program EStatementArrivalEmail End");
                if (reader != null && !reader.IsClosed) reader.Close();
                if (readerProductInfo != null && !readerProductInfo.IsClosed) readerProductInfo.Close();
                if (conn != null && (conn.State & ConnectionState.Open) != 0) conn.Close();
                System.Threading.Thread.Sleep(5000);
            }
        }

        //20160523 add by Pong
        public static UNE_WS.SendMessageResponse test()
        {
            String appID = "ITD";
            String templateID = "ESMARVL001";
            String appCountryCode = "HK";
            String emailAddress = "kinpong_lam@cncbinternational.com";
            String msgVariables = "{}";
            String appRef = "";


            UNE_WS.MessageService ws = new UNE_WS.MessageService();
            UNE_WS.SendMessageRequest sendMessageRequest = new UNE_WS.SendMessageRequest();
            UNE_WS.MessageDetail mDetails = new UNE_WS.MessageDetail();
            UNE_WS.MessageDestination mDestination = new UNE_WS.MessageDestination();
            UNE_WS.Email email = new UNE_WS.Email();


            email.emailAddress = emailAddress;
            Email[] emailArr = new Email[1];
            emailArr[0] = email;

            mDestination.emails = emailArr;
            mDetails.destination = mDestination;
            mDetails.msgVariables = msgVariables;

            sendMessageRequest.appId = appID;
            sendMessageRequest.templateId = templateID;
            sendMessageRequest.appCountryCode = appCountryCode;
            sendMessageRequest.appRef = appRef;

            sendMessageRequest.messageDetail = mDetails;

            UNE_WS.SendMessageResponse response = ws.sendMessage(sendMessageRequest);

            Console.WriteLine("SendByUNE: request: " + sendMessageRequest.messageDetail.destination.emails[0].emailAddress);
            Console.WriteLine("SendByUNE: request: " + sendMessageRequest.templateId);
            Console.WriteLine("SendByUNE: ws.response : " + response.refNo + " " + response.status + " " + response.returnCode + " " + response.returnMsg.ToString());


            return response;

        }

        private static String testJSON()
        {

            MsgVariables msgVariables = new MsgVariables("99", "Infocast Transaction Statement", "Infocast Transaction Statement Chi..", "1234-1234-1234-1234");
            string json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(msgVariables);

            return json;
        }



        public static void Log(logType type, String content, SqlConnection conn)
        {
            try
            {
                DateTime now = DateTime.Now;
                String query = "INSERT INTO tbLog (Type, Content, CreateDate, CreateBy, ModifyBy, LastUpdate) VALUES(@Type, @Content, @CreateDate, @CreateBy, @ModifyBy, @LastUpdate)";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.Add("@Type", Enum.GetName(typeof(logType), type));
                //command.Parameters.Add("@Content", content);
                if (content.Length < 900)
                {
                    command.Parameters.Add("@Content", content);
                }
                else
                {
                    command.Parameters.Add("@Content", "Content Out of Database Input Range, Content truncated to 900 characters for reference: " + content.Substring(0, 900));
                }
                command.Parameters.Add("@CreateDate", now);
                command.Parameters.Add("@CreateBy", Properties.Settings.Default.AppName);
                command.Parameters.Add("@ModifyBy", Properties.Settings.Default.AppName);
                command.Parameters.Add("@LastUpdate", now);

                command.ExecuteNonQuery();
                if (type != logType.Warning)
                {
                    Console.WriteLine(content);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Log Insert Database Error. " + "Reason: " + e.Message.ToString());
            }
        }

        public static void writeLog(String content)
        {
            try
            {
                if (File.Exists(logPath))
                {
                    if (!File.GetLastWriteTime(logPath).ToString("yyyyMMdd").Equals(DateTime.Now.ToString("yyyyMMdd")))
                    {
                        String newLogPath = String.Format("{0}_{1:yyyyMMdd}.log", logPath.Replace(".log", ""), File.GetLastWriteTime(logPath).ToString("yyyyMMdd"));
                        File.Delete(newLogPath);
                        File.Move(logPath, newLogPath);
                    }
                }
                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    sw.Close();
                }

                //Log(logType.Info, content);
            }
            catch (Exception e)
            {
                Console.WriteLine("Write Log File Error. " + "Reason: " + e.Message.ToString());
            }

        }

    }



    class NotificationEmail
    {
        /*
        static String SmtpHost = ConfigurationSettings.AppSettings["SmtpServer"];
        static int SmtpPort = Int32.Parse(ConfigurationSettings.AppSettings["SmtpPort"]);
        static String sender = ConfigurationSettings.AppSettings["SenderAddress"];
        */

        //20161121 add sender address in ws
        static String sender = ConfigurationSettings.AppSettings["SenderAddress"];

        public String RefNo { get; private set; }
        public String EMailRef { get; private set; }
        String category;
        String engDesc;
        String tradChiDesc;
        String displayAccountNumber;
        //public String Subject { get; private set; }
        //public String Content { get; private set; }

        //20160523 add by Pong
        public String json;

        //For Normal Case
        public NotificationEmail(String refNoSBS, String refNo, String category, String engDesc, String tradChiDesc, String displayAccountNumber)
        {

            Console.WriteLine("NotificationEmail: Start");

            if (String.IsNullOrEmpty(refNoSBS))
            {
                this.RefNo = refNo;
            }
            else 
            {
                this.RefNo = "S" + refNoSBS;
            }

            this.EMailRef = Guid.NewGuid().ToString();
            this.category = category;
            this.engDesc = engDesc;
            this.tradChiDesc = tradChiDesc;
            this.displayAccountNumber = displayAccountNumber;
            //20160523 comment
            //this.Subject = "Your CNCBI eStatement is now available 您的信銀國際電子結單已備妥  [Ref. " + refNo + "]";
            //this.Content = GetEMailContent(category, engDesc, tradChiDesc, displayAccountNumber);

            //20160523 add by Pong

            Console.WriteLine("NotificationEmail: prepareMsgVariable" + displayAccountNumber);
            this.json = prepareMsgVariable(category, engDesc, tradChiDesc, displayAccountNumber);
        }

        //For SBS AVQ A Case
        public NotificationEmail(String refNoSBS, String refNo, String category, String engDesc, String tradChiDesc, String displayAccountNumber, String Advice_Title, String Product_Name, String Product_Type)
        {

            Console.WriteLine("NotificationEmail: Start");

            if (String.IsNullOrEmpty(refNoSBS))
            {
                this.RefNo = refNo;
            }
            else
            {
                this.RefNo = "S" + refNoSBS;
            }

            this.EMailRef = Guid.NewGuid().ToString();
            this.category = category;
            this.engDesc = engDesc;
            this.tradChiDesc = tradChiDesc;
            this.displayAccountNumber = displayAccountNumber;

            Console.WriteLine("NotificationEmail: prepareMsgVariable For SBS" + displayAccountNumber);
            this.json = prepareMsgVariable(category, Advice_Title, Product_Name, Product_Type, displayAccountNumber);
        }


        private static String GetMaskedAccountNo(String category, String accountNo)
        {
            switch (category)
            {
                case "CiticFirst":
                    return "";
                case "SBS":
                    return accountNo;
                case "AVQ":
                    return accountNo;
                case "CreditCard":
                    return "****-****-****-" + accountNo.Substring(accountNo.Length - 4);
                case "FixDeposit": //Case Direct Bank Hard Code Ver
                    return accountNo.Substring(0, accountNo.Length - 11) + new String('*', 5) + accountNo.Substring(accountNo.Length-6, 6);
                case "InfoCast":
                default:
                    return accountNo.Substring(0, accountNo.Length - 5) + new String('*', 5);
            }
        }

        //For Normal Case
        //20160523 add by Pong
        private String prepareMsgVariable(String category, String statementDescEng, String statementDescTChi, String accountNo)
        {

            //{"var_ref_no" : "694123456789","var_statement_desc_eng" : "Statement XXXX","var_statement_desc_tchi" : "Statement @@@@","var_mask_account_no" : "123445321"}
            String maskAccountNo = GetMaskedAccountNo(category, accountNo.Trim());
            Console.WriteLine("prepareMsgVariable: " + maskAccountNo);
            MsgVariables msgVariables = new MsgVariables(RefNo, statementDescEng, statementDescTChi, maskAccountNo);

            string json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(msgVariables);
            return json;
        }

        //For SBS AVQ A Case
        private String prepareMsgVariable(String category, String Advice_Title, String Product_Name, String Product_Type, String accountNo)
        {
            String maskAccountNo = GetMaskedAccountNo(category, accountNo.Trim());
            Console.WriteLine("prepareMsgVariable For SBS: " + maskAccountNo);
            MsgVariables msgVariables = new MsgVariables(RefNo, Advice_Title, Product_Name, Product_Type, maskAccountNo);

            string json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(msgVariables);
            return json;
        }

        public UNE_WS.SendMessageResponse SendByUNE(String appID, String templateID, String appCountryCode, String emailAddress, String mobileNo, int eStatementID, String PreferredLanguage, String flag, SqlConnection conn)
        {

            if (Properties.Settings.Default.SkipUNE.Equals("Y"))
            {
                //Console.WriteLine("SkipUNE");
                UNE_WS.SendMessageResponse testResponse = new SendMessageResponse();
                testResponse.refNo = DateTime.Now.ToString("yyyyMMddhhmmssffffff");
                testResponse.returnMsg = null;
                testResponse.status = SendMessageResponseStatus.A;
                EStatementArrivalEmail.Program.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip All UNE Send Message Case= eStatementID: " + eStatementID + " UNE Reference Number: " + testResponse.refNo + " Email Address: " + emailAddress + " Mobile Number: " + mobileNo), conn);
                EStatementArrivalEmail.Program.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip All UNE Send Message Case= eStatementID: " + eStatementID + " UNE Reference Number: " + testResponse.refNo + " Email Address: " + emailAddress + " Mobile Number: " + mobileNo);
                return testResponse;
            }
            else if (Properties.Settings.Default.SkipUNE.Equals("B") && flag.Equals("R"))
            {
                //Console.WriteLine("SkipUNE Bounce Back Case");
                UNE_WS.SendMessageResponse testResponse = new SendMessageResponse();
                testResponse.refNo = DateTime.Now.ToString("yyyyMMddhhmmssfff");
                testResponse.returnMsg = null;
                testResponse.status = SendMessageResponseStatus.A;
                EStatementArrivalEmail.Program.Log(logType.Info, String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip Send Bounce Back EMail and SMS Message Case= eStatementID: " + eStatementID + " UNE Reference Number: " + testResponse.refNo + " Email Address: " + emailAddress + " Mobile Number: " + mobileNo), conn);
                EStatementArrivalEmail.Program.writeLog("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Skip Send Bounce Back EMail and SMS Message Case= eStatementID: " + eStatementID + " UNE Reference Number: " + testResponse.refNo + " Email Address: " + emailAddress + " Mobile Number: " + mobileNo);
                return testResponse;
            }


            String msgVariables = json;
            int appRef = eStatementID;


            UNE_WS.MessageService ws = new UNE_WS.MessageService();
            UNE_WS.SendMessageRequest sendMessageRequest = new UNE_WS.SendMessageRequest();
            UNE_WS.MessageDetail mDetails = new UNE_WS.MessageDetail();
            UNE_WS.MessageDestination mDestination = new UNE_WS.MessageDestination();
            UNE_WS.Email email = new UNE_WS.Email();
            // Morris SMS trial
            UNE_WS.SMS sms = new UNE_WS.SMS();
            if (!String.IsNullOrEmpty(mobileNo))
            {
                // SMS
                sms.mobileNo = mobileNo;
                sms.forwardingType = SMSForwardingType.Item1;
                sms.langCode = langCode.en_US;

                if (PreferredLanguage.Equals("Traditional Chinese"))
                {
                    sms.langCode = langCode.zh_HK;
                }
                else if (PreferredLanguage.Equals("Simplified Chinese"))
                {
                    sms.langCode = langCode.zh_CN;
                }

                sms.langCodeSpecified = true;
                SMS[] smslArr = new SMS[1];
                smslArr[0] = sms;
                mDestination.smss = smslArr;
                mDetails.destination = mDestination;
                mDetails.msgVariables = msgVariables;
            }
            else
            {
                // Email
                email.emailAddress = emailAddress;
                //20161121 add sender address in ws
                email.fromEmailAddress = sender;
                //end 20161121

                Email[] emailArr = new Email[1];
                emailArr[0] = email;
                mDestination.emails = emailArr;
                mDetails.destination = mDestination;
                mDetails.msgVariables = msgVariables;
            }

            sendMessageRequest.appId = appID;
            sendMessageRequest.templateId = templateID;
            sendMessageRequest.appCountryCode = appCountryCode;
            sendMessageRequest.appRef = appRef.ToString();

            sendMessageRequest.messageDetail = mDetails;

            UNE_WS.SendMessageResponse response = ws.sendMessage(sendMessageRequest);

            //Console.WriteLine("SendByUNE: request: " + sendMessageRequest.messageDetail.destination.emails[0].emailAddress);
            //Console.WriteLine("SendByUNE: request: " + sendMessageRequest.templateId);
            Console.WriteLine("SendByUNE: ws.response : " + appRef + " " + response.refNo + " " + response.status + " " + response.returnCode + " " + response.returnMsg);

            return response;

        }

        //20160523 comment
        /*
        private static String GetEMailContent(String category, String statementDescEng, String statementDescTChi, String accountNo)
        {
            String maskAccountNo = GetMaskedAccountNo(category, accountNo);

            String content = "Dear Customer,\n\n" +
                "The latest eStatement(s) for your below account(s) is / are now available:\n\n" +
                "- " + statementDescEng + " " + maskAccountNo + "\n\n" +
                "You can login to i-banking to view your statement details and other important notices.\n\n" +
                "Important Note:\n" +
                "Customers who have subscribed to use eStatement service will not receive paper statements, please check your eStatement immediately:\n" +
                "- To avoid additional fees or charges incurred for late payments (e.g. outstanding balance of credit card);\n" +
                "- To notify the Bank as soon as possible if there is any error or discrepancy.\n\n" +
                "Should you have any queries, please call our Direct Banking Hotline (852) 2287-6767 for assistance (press \"0\" after language selection)*.\n\n" +
                "Thank you for using our eStatement service.\n\n" +
                "China CITIC Bank International Limited\n\n" +
                "* Direct Banking Service Hours :\n" +
                "  Mondays to Fridays : 9:00 a.m. to 8:00 p.m.\n" +
                "  Saturdays : 9:00 a.m. to 6:00 p.m. (except public holidays)\n\n" +
                "- Please do not reply this email -\n\n" +
                "親愛的客戶：\n\n" +
                "您以下戶口的最新電子結單已備妥:\n\n" +
                "- " + statementDescTChi + " " + maskAccountNo + "\n\n" +
                "您可登入網上理財，以瀏覽結單詳情及其他重要通知。\n\n" +
                "重要聲明：\n" +
                "選用電子結單服務的客戶將不會收到郵寄結單，請立即查閱您的電子結單:\n" +
                "– 避免因逾期繳賬(如信用卡總結欠)而產生額外的費用；\n" +
                "– 如發現錯漏或不符，請即通知本行。\n\n" +
                "如有查詢，請致電電話理財專線(852) 2287-6767 (選擇語言後按\"0\"字)與本行職員聯絡*。\n\n" +
                "多謝您選用我們的電子結單服務。\n\n" +
                "中信銀行（國際）有限公司\n\n" +
                "* 電話理財專線的服務時間：\n" +
                "  星期一至五：早上9:00 至晚上 8:00\n" +
                "  星期六：早上9:00至晚上6:00 (公眾假期除外)\n\n" +
                "- 請不要回覆此電郵 -";

            return content;
        }

        public void Send(String destination)
        {
            SmtpClient client = new SmtpClient(SmtpHost, SmtpPort);

            MailMessage msg = new MailMessage(sender, destination);
            msg.Subject = Subject;
            msg.SubjectEncoding = Encoding.UTF8;
            msg.Body = Content;
            msg.BodyEncoding = Encoding.UTF8;
            msg.Headers.Add("Message-ID", EMailRef);
            client.Send(msg);
        }

        public void SendDummy(String destination)
        {
            Debug.WriteLine("Destination:" + destination);
            Debug.WriteLine("Subject:" + Subject);
            Debug.WriteLine("Body:" + Content);
        }
         */
    }


    //20160523 add by Pong
    class MsgVariables
    {
        //{"var_ref_no" : "694123456789","var_statement_desc_eng" : "Statement XXXX","var_statement_desc_tchi" : "Statement @@@@","var_mask_account_no" : "123445321"}
        public String var_ref_no { get; private set; }
        public String var_statement_desc_eng { get; private set; }
        public String var_statement_desc_tchi { get; private set; }
        public String var_mask_account_no { get; private set; }
        public String var_advice_title { get; private set; }
        public String var_product_name { get; private set; }
        public String var_product_type { get; private set; }

        //For Normal Case
        public MsgVariables(String refNo, String descEng, String descTC, String maskAccNo)
        {
            this.var_ref_no = refNo;
            this.var_statement_desc_eng = descEng;
            this.var_statement_desc_tchi = descTC;
            this.var_mask_account_no = maskAccNo;
        }

        //For SBS Case
        public MsgVariables(String refNo, String adviceTitle, String productName, String productType, String maskAccNo)
        {
            this.var_ref_no = refNo;
            this.var_advice_title = adviceTitle;
            this.var_product_name = productName;
            this.var_product_type = productType;
            this.var_mask_account_no = maskAccNo;
        }
    }

    static class BOUNCEBACK_STATUS
    {
        public const string FIRSTEMAIL = "E1";
        public const string SECONDEMAIL = "E2";
        public const string THIRDEMAIL = "E3";
        public const string FIRSTSMS = "S1";
        public const string SECONDSMS = "S2";
        public const string THIRDSMS = "S3";
        public const string PapaerStatment = "PS";
        public const string NonInvestSMSOTP = "NIS";
    }

}
