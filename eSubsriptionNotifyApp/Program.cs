﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using eSubsriptionNotifyApp.UNE_WS;
using System.Web.Script;
using System.IO;

namespace eSubsriptionNotifyApp
{
    public enum logType { Info, Error, Warning };


    class Program
    {
        static String logFilename = String.Format("{0}_{1:yyyy-MM-dd}.log", Properties.Settings.Default.AppName, DateTime.Now);
        static String logPath = Path.Combine(Properties.Settings.Default.LogPath, logFilename);
        static EstatementEntities Entity = new EstatementEntities();


        static void Main(string[] args)
        {
            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["DBConnectString"]);
            //There is already an open DataReader associated with this Command which must be closed first  
            //MultipleActiveResultSets=true and change view 
            SqlDataReader reader = null;

            /*
             * Status List
                S: Success (New Account)
                B: No EMail and Moblie in tbCustomerEmail record
                C: Record Not Found in tbCustomerEMail
                T: Record Not Found in tbStatementType
                F: No Change in Registration Status 
                N: Record in tbAccountList Active = N
             */
            try
            {
                conn.Open();
                Logs(logType.Info, String.Format("Program eSubsriptionNotifyApp Start"));
                String emailTemplateID = Properties.Settings.Default.EMailTemplateID;
                String smsTemplateID = Properties.Settings.Default.SMSTemplateID;
                String regStatusBefore;
                String regStatusAfter;

                var tbAccountNoticeLists = Entity.tbAccountNoticeList.Where(a => a.TriggerStatus == "P");
                var tbAccountLists = Entity.tbAccountList.Where(a => a.Active == "Y").ToList();

                foreach (tbAccountNoticeList tbAccountNotice in tbAccountNoticeLists.ToList())
                {
                    try{

                        if (tbAccountLists.Exists(a => a.RMID == tbAccountNotice.RMID && a.AccountNumber == tbAccountNotice.AccountNumber && a.Category == tbAccountNotice.Category && a.StatementType == tbAccountNotice.StatementType))
                        {
                            var realtbAccountList = tbAccountLists.Where(a => a.RMID == tbAccountNotice.RMID && a.AccountNumber == tbAccountNotice.AccountNumber && a.Category == tbAccountNotice.Category && a.StatementType == tbAccountNotice.StatementType).FirstOrDefault();
                            tbAccountNotice.AccountID = realtbAccountList.ID;

                            if (String.IsNullOrEmpty(tbAccountNotice.RegistrationStatusBefore))
                            {
                                regStatusBefore = "";
                            }
                            else
                            {
                                regStatusBefore = tbAccountNotice.RegistrationStatusBefore;
                            }

                            if (String.IsNullOrEmpty(tbAccountNotice.RegistrationStatusAfter))
                            {
                                regStatusAfter = "";
                            }
                            else
                            {
                                regStatusAfter = tbAccountNotice.RegistrationStatusAfter;
                            }

                            if (!regStatusBefore.Equals(regStatusAfter))
                            {
                                //check tbStatementType if null 
                                tbStatementType statementTypeData = Entity.tbStatementType.Where(a => a.StatementType.Equals(tbAccountNotice.StatementType)).FirstOrDefault();

                                if (statementTypeData != null)
                                {
                                    SqlCommand cmdSelect = new SqlCommand("SELECT TOP 1 EMailAddress, MobileNumber, AcctStatus, AcctStatusLastUpdDt, PreferredLanguage FROM tbCustomerEMail WHERE RMID = @RMID", conn);
                                    cmdSelect.Parameters.AddWithValue("@RMID", tbAccountNotice.RMID);
                                    reader = cmdSelect.ExecuteReader();

                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            string statementTypeDesc = "";
                                            if (reader["PreferredLanguage"] != System.DBNull.Value)
                                            {
                                                string preferredLanguage = (string)reader["PreferredLanguage"];

                                                if (preferredLanguage.Equals("Traditional Chinese"))
                                                {
                                                    statementTypeDesc = statementTypeData.TradChiDesc;
                                                }
                                                else if (preferredLanguage.Equals("Simplified Chinese"))
                                                {
                                                    statementTypeDesc = statementTypeData.SimChiDesc;
                                                }
                                                else
                                                {
                                                    statementTypeDesc = statementTypeData.EngDesc;
                                                }
                                            }
                                            else 
                                            {
                                                statementTypeDesc = statementTypeData.EngDesc;
                                            }

                                            NotificationEmail email = new NotificationEmail(tbAccountNotice.Category, statementTypeDesc, tbAccountNotice.DisplayAccountNumber);

                                            bool EMailSMSChecking = false;

                                            //add if Email null handle
                                            if (reader["EMailAddress"] != System.DBNull.Value)
                                            {
                                                if (!String.IsNullOrEmpty((string)reader["EMailAddress"]))
                                                {
                                                    UNE_WS.SendMessageResponse emailResult = email.SendByUNE("ES", emailTemplateID, "HK", (string)reader["EMailAddress"], "", realtbAccountList.ID, (string)reader["PreferredLanguage"]);
                                                    tbAccountNotice.TriggerStatus = "S";
                                                    tbAccountNotice.LastUpdate = System.DateTime.Now;
                                                    EMailSMSChecking = true;
                                                    System.Threading.Thread.Sleep(Int32.Parse(Properties.Settings.Default.EMailTriggerDelayTime));
                                                }else
                                                {
                                                    Logs(logType.Error, String.Format("AccountID: " + realtbAccountList.ID + " RMID: " + tbAccountNotice.RMID + " Trigger EMail Notification Failed" + " Reason: Empty EMail Address in tbCustomerEMail Record"));
                                                }
                                            }
                                            else
                                            {
                                                //tbAccountNotice.TriggerStatus = "E";
                                                Logs(logType.Error, String.Format("AccountID: " + realtbAccountList.ID + " RMID: " + tbAccountNotice.RMID + " Trigger EMail Notification Failed" + " Reason: Empty EMail Address in tbCustomerEMail Record"));
                                            }

                                            //add if mobile null handle

                                            if (reader["MobileNumber"] != System.DBNull.Value)
                                            {
                                                if (!String.IsNullOrEmpty((string)reader["MobileNumber"]))
                                                {
                                                    UNE_WS.SendMessageResponse smsResult = email.SendByUNE("ES", smsTemplateID, "HK", "", (string)reader["MobileNumber"], realtbAccountList.ID, (string)reader["PreferredLanguage"]);
                                                    tbAccountNotice.TriggerStatus = "S";
                                                    tbAccountNotice.LastUpdate = System.DateTime.Now;
                                                    EMailSMSChecking = true;
                                                    System.Threading.Thread.Sleep(Int32.Parse(Properties.Settings.Default.SMSTriggerDelayTime));
                                                }
                                                else 
                                                {
                                                    Logs(logType.Error, String.Format("AccountID: " + realtbAccountList.ID + " RMID: " + tbAccountNotice.RMID + " Trigger SMS Notification Failed" + " Reason: Empty Mobile Number in tbCustomerEMail Record"));
                                                }
                                            }
                                            else
                                            {
                                                //tbAccountNotice.TriggerStatus = "M";
                                                Logs(logType.Error, String.Format("AccountID: " + realtbAccountList.ID + " RMID: " + tbAccountNotice.RMID + " Trigger SMS Notification Failed" + " Reason: Empty Mobile Number in tbCustomerEMail Record"));
                                            }

                                            if (!EMailSMSChecking)
                                            {
                                                tbAccountNotice.TriggerStatus = "B";
                                                tbAccountNotice.LastUpdate = System.DateTime.Now;
                                                Logs(logType.Error, String.Format("AccountID: " + realtbAccountList.ID + " RMID: " + tbAccountNotice.RMID + " Trigger EMail and SMS Notification Failed" + " Reason: Empty EMail and Moblie Number in tbCustomerEMail Record"));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        tbAccountNotice.TriggerStatus = "C";
                                        tbAccountNotice.LastUpdate = System.DateTime.Now;
                                        Logs(logType.Error, String.Format("AccountID: " + realtbAccountList.ID + " RMID: " + tbAccountNotice.RMID + " Trigger EMail and SMS Notification Failed" + " Reason: Record Not Found in tbCustomerEMail"));
                                    }
                                }
                                else
                                {
                                    tbAccountNotice.TriggerStatus = "T";
                                    tbAccountNotice.LastUpdate = System.DateTime.Now;
                                    Logs(logType.Error, String.Format("AccountID: " + realtbAccountList.ID + " RMID: " + tbAccountNotice.RMID + " StatementType: " + tbAccountNotice.StatementType + " Trigger EMail and SMS Notification Failed" + " Reason: Record Not Found in tbStatementType"));
                                }
                            }
                            else
                            {
                                tbAccountNotice.TriggerStatus = "F";
                                tbAccountNotice.LastUpdate = System.DateTime.Now;
                            }
                        }
                        else 
                        {
                            tbAccountNotice.TriggerStatus = "N";
                            tbAccountNotice.LastUpdate = System.DateTime.Now;
                            Logs(logType.Info, String.Format("AccountNo: " + tbAccountNotice.AccountNumber + " Category: " + tbAccountNotice.Category + " RMID: " + tbAccountNotice.RMID + " StatementType: " + tbAccountNotice.StatementType + " Skip Case" + " Reason: Record in tbAccountList Active = N"));
                        }
                        
                        
                        Entity.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        String result = ex.Message.ToString();
                        Logs(logType.Error, String.Format("Unexpected Error: " + result));
                    }
                }
            }
            catch (Exception ex)
            {
                String result = ex.Message.ToString();
                Logs(logType.Error, String.Format("Unexpected Error: " + result));
            }
            finally
            {
                Logs(logType.Info, String.Format("Program eSubsriptionNotifyApp End"));
                ////Entity.ExecuteStoreCommand("TRUNCATE TABLE tbAccountNoticeList"); Keep Record, No Need to Truncate table
                if (reader != null && !reader.IsClosed) reader.Close();
                if (conn != null && (conn.State & ConnectionState.Open) != 0) conn.Close();
                System.Threading.Thread.Sleep(5000);
            }
        }

        public static void Log(logType type, String content)
        {
            try
            {
                content = "[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [" +Enum.GetName(typeof(logType), type)+ "] " + content;

                DateTime now = DateTime.Now;
                tbLog log = new tbLog();
                log.Type = Enum.GetName(typeof(logType), type);
                if (content.Length < 900)
                {
                    log.Content = content;
                }
                else
                {
                    log.Content = content.Substring(0, 900);
                    log.Content = "Content Out of Database Input Range, Content truncated to 900 characters for reference: " + log.Content;
                }

                log.CreateBy = Properties.Settings.Default.AppName;
                log.ModifyBy = Properties.Settings.Default.AppName;
                log.CreateDate = now;
                log.LastUpdate = now;
                Entity.AddTotbLog(log);
                Entity.SaveChanges();

                if (type != logType.Warning)
                {
                    Console.WriteLine(content);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Log Insert Database Error. " + "Reason: " + e.Message.ToString());
            }
        }

        public static void writeLog(logType type, String content)
        {
            try
            {
                content = "[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [" +Enum.GetName(typeof(logType), type)+ "] "+ content;
                if (File.Exists(logPath))
                {
                    if (!File.GetLastWriteTime(logPath).ToString("yyyyMMdd").Equals(DateTime.Now.ToString("yyyyMMdd")))
                    {
                        String newLogPath = String.Format("{0}_{1:yyyyMMdd}.log", logPath.Replace(".log", ""), File.GetLastWriteTime(logPath).ToString("yyyyMMdd"));
                        File.Delete(newLogPath);
                        File.Move(logPath, newLogPath);
                    }
                }
                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    sw.Close();
                }

                //Log(logType.Info, content);
            }
            catch (Exception e)
            {
                Console.WriteLine("Write Log File Error. " + "Reason: " + e.Message.ToString());
            }

        }

        public static void Logs(logType type, String content)
        {
            Log(type, content);
            writeLog(type, content);
        }

    }



    class NotificationEmail
    {
        /*
        static String SmtpHost = ConfigurationSettings.AppSettings["SmtpServer"];
        static int SmtpPort = Int32.Parse(ConfigurationSettings.AppSettings["SmtpPort"]);
        static String sender = ConfigurationSettings.AppSettings["SenderAddress"];
        */

        //20161121 add sender address in ws
        static String sender = ConfigurationSettings.AppSettings["SenderAddress"];

        public int RefNo { get; private set; }
        public String EMailRef { get; private set; }
        String category;
        String engDesc;
        String tradChiDesc;
        String displayAccountNumber;
        //public String Subject { get; private set; }
        //public String Content { get; private set; }

        //20160523 add by Pong
        public String json;

        public NotificationEmail(String category, String statement_type, String displayAccountNumber)
        {

            Console.WriteLine("NotificationEmail: Start");

            this.EMailRef = Guid.NewGuid().ToString();
            this.category = category;
            this.displayAccountNumber = displayAccountNumber;
            //20160523 comment
            //this.Subject = "Your CNCBI eStatement is now available 您的信銀國際電子結單已備妥  [Ref. " + refNo + "]";
            //this.Content = GetEMailContent(category, engDesc, tradChiDesc, displayAccountNumber);

            //20160523 add by Pong
            Console.WriteLine("NotificationEmail: prepareMsgVariable" + displayAccountNumber);
            this.json = prepareMsgVariable(category, statement_type, displayAccountNumber);
        }

        private static String GetMaskedAccountNo(String category, String accountNo)
        {
            switch (category)
            {
                case "CiticFirst":
                    return "";
                case "CreditCard":
                    return "****-****-****-" + accountNo.Substring(accountNo.Length - 4);
                case "InfoCast":
                default:
                    return accountNo.Substring(0, accountNo.Length - 5) + new String('*', 5);
            }
        }

        //20160523 add by Pong
        private String prepareMsgVariable(String category, String statement_type, String accountNo)
        {
            String maskAccountNo = GetMaskedAccountNo(category, accountNo);
            Console.WriteLine("Prepare Msg Variable: " + maskAccountNo);
            MsgVariables msgVariables = new MsgVariables(statement_type, maskAccountNo);

            string json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(msgVariables);
            return json;
        }

        public UNE_WS.SendMessageResponse SendByUNE(String appID, String templateID, String appCountryCode, String emailAddress, String mobileNo, int accountID, String PreferredLanguage)
        {
            if (Properties.Settings.Default.SkipUNE.Equals("Y"))
            {
                //Console.WriteLine("SkipUNE");
                UNE_WS.SendMessageResponse testResponse = new SendMessageResponse();
                testResponse.refNo = DateTime.Now.ToString("yyyyMMddhhmmssffffff");
                testResponse.returnMsg = null;
                testResponse.status = SendMessageResponseStatus.A;
                eSubsriptionNotifyApp.Program.Logs(logType.Info, String.Format("Skip UNE Send Message Case= AccountID: " + accountID + " UNE Reference Number: " + testResponse.refNo + " Email Address: " + emailAddress + " Mobile Number: " + mobileNo));
                return testResponse;
            }

            PreferredLanguage = PreferredLanguage.Trim();

            String msgVariables = json;
            int appRef = accountID;

            UNE_WS.MessageService ws = new UNE_WS.MessageService();
            UNE_WS.SendMessageRequest sendMessageRequest = new UNE_WS.SendMessageRequest();
            UNE_WS.MessageDetail mDetails = new UNE_WS.MessageDetail();
            UNE_WS.MessageDestination mDestination = new UNE_WS.MessageDestination();
            UNE_WS.Email email = new UNE_WS.Email();
            // Morris SMS trial
            UNE_WS.SMS sms = new UNE_WS.SMS();
            if (!String.IsNullOrEmpty(mobileNo))
            {
                // SMS
                sms.mobileNo = mobileNo;
                sms.forwardingType = SMSForwardingType.Item1;
                sms.langCode = langCode.en_US;

                if (PreferredLanguage.Equals("Traditional Chinese"))
                {
                    sms.langCode = langCode.zh_HK;
                }
                else if (PreferredLanguage.Equals("Simplified Chinese"))
                {
                    sms.langCode = langCode.zh_CN;
                }

                sms.langCodeSpecified = true;
                SMS[] smslArr = new SMS[1];
                smslArr[0] = sms;
                mDestination.smss = smslArr;
                mDetails.destination = mDestination;
                mDetails.msgVariables = msgVariables;
                eSubsriptionNotifyApp.Program.Logs(logType.Info, String.Format("UNE Sending AccountID: " + accountID + " Type: SMS " + "langCode: " + sms.langCode.ToString()));
            }
            else
            {
                // Email
                email.emailAddress = emailAddress;
                //20161121 add sender address in ws
                email.fromEmailAddress = sender;
                //end 20161121

                email.langCode = langCode.en_US;

                if (PreferredLanguage.Equals("Traditional Chinese"))
                {
                    email.langCode = langCode.zh_HK;
                }
                else if (PreferredLanguage.Equals("Simplified Chinese"))
                {
                    email.langCode = langCode.zh_CN;
                }

                email.langCodeSpecified = true;
                Email[] emailArr = new Email[1];
                emailArr[0] = email;
                mDestination.emails = emailArr;
                mDetails.destination = mDestination;
                mDetails.msgVariables = msgVariables;
                eSubsriptionNotifyApp.Program.Logs(logType.Info, String.Format("UNE Sending AccountID: " + accountID + " Type: EMail " + "langCode: " + email.langCode.ToString()));
            }

            sendMessageRequest.appId = appID;
            sendMessageRequest.templateId = templateID;
            sendMessageRequest.appCountryCode = appCountryCode;
            sendMessageRequest.appRef = appRef.ToString();
            sendMessageRequest.messageDetail = mDetails;
            
            UNE_WS.SendMessageResponse response = ws.sendMessage(sendMessageRequest);
            
            Console.WriteLine("SendByUNE: ws.response : " + appRef + " " + response.refNo + " " + response.status + " " + response.returnCode + " " + response.returnMsg);

            return response;

        }
    }

    //20160523 add by Pong
    class MsgVariables
    {
        public String statement_type { get; private set; }
        public String acct_no { get; private set; }
        public String dt_email_gen { get; private set; }

        public MsgVariables(String statement_type, String acct_no)
        {
            this.statement_type = statement_type;
            this.acct_no = acct_no;
            this.dt_email_gen = DateTime.Now.ToString("yyyyMMdd");
        }
    }

}
