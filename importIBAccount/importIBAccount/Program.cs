﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace ImportIBAccount
{
    class Program
    {
        static int Main(string[] args)
        {
            StreamReader sr = null;
            SqlConnection conn = null;
            IFormatProvider culture = new CultureInfo("en-US", true);

            try
            {
                String[] ibFilenames = Directory.GetFiles(ConfigurationSettings.AppSettings["IBFilePath"], ConfigurationSettings.AppSettings["IBFilePattern"]);

                if (ibFilenames.Length == 0)
                {
                    Console.WriteLine("No IB file");
                    return 1;
                }

                Array.Sort(ibFilenames);

                sr = File.OpenText(ibFilenames.Last());

                String lineData = sr.ReadLine();

                if (!lineData.Equals("Master Profile Report"))
                {
                    Console.WriteLine("Invalid IB File - file name");
                    return 2;
                }

                lineData = sr.ReadLine();
                if (lineData.Length != 0)
                {
                    Console.WriteLine("Invalid IB File - linefeed");
                    return 2;
                }

                lineData = sr.ReadLine();
                //20170412 Update Header checking 479 to 620
                if (!lineData.Substring(0, 620).Equals("IB ACCT NO,ID NO,CUSTOMER RATE CLASS,CUSTOMER LIMIT CLASS,LAST LOGIN DATE,LAST SUCCESSFUL LOGIN DATE,LAST FAILED LOGIN DATE,IMPROPER LOGOUT FLAG,BRANCH ACTIVATION KEY,BSC ACTIVATION KEY,CUSTOMER ACTIVATION KEY,BRANCH DATE TIME,BSC_DATETIME,CUSTOMER DATE TIME,LAST STATUS DATE,ACC STATUS CODE,ACCOUNT STATUS DATE,STATUS REASON,APPLICATION CHANNEL,APPLICATION DATE,FAILED LOGIN COUNT,PIN LIFESPAN,DISCLAIMER FLAG,DISCLAIMER DATE,T&C FLAG,T&C DATE,SK T&C FLAG,SK T&C DATE,EMAIL ADDR, MOBILE PHONE, LANG, SOURCE CODE,HARD TOKEN STATUS, HARD TOKEN STATUS CHANGE DATE, SOFT TOKEN STATUS, SOFT TOKEN STATUS CHANGE DATE, SMS OTP"))
                {
                    Console.WriteLine("Invalid IB File - header");
                    return 2;
                }

                conn = new SqlConnection(ConfigurationSettings.AppSettings["DBConnectString"]);
                conn.Open();

                SqlTransaction tran = conn.BeginTransaction();

                SqlCommand cmd = new SqlCommand("DELETE FROM tbCustomerEMail", conn, tran);
                cmd.ExecuteNonQuery();

                cmd = new SqlCommand("INSERT INTO tbCustomerEmail (RMID, EMailAddress, MobileNumber, PreferredLanguage, AcctStatus, AcctStatusLastUpdDt) VALUES (@RMID, @EMailAddress, @MobileNumber, @PreferredLanguage, @AcctStatus, @AcctStatusLastUpdDt)", conn, tran);
                cmd.Parameters.Add("@RMID", SqlDbType.NVarChar);
                cmd.Parameters.Add("@EMailAddress", SqlDbType.NVarChar);
                cmd.Parameters.Add("@MobileNumber", SqlDbType.NVarChar);
                cmd.Parameters.Add("@PreferredLanguage", SqlDbType.NVarChar);
                cmd.Parameters.Add("@AcctStatus", SqlDbType.VarChar);
                cmd.Parameters.Add("@AcctStatusLastUpdDt", SqlDbType.DateTime);

                int lineCount = 0;

                while (!sr.EndOfStream)
                {
                    lineData = sr.ReadLine();

                    if (lineData.Length == 0)
                        break;

                    String[] fieldValues = lineData.Split(',');
                    cmd.Parameters["@RMID"].Value = fieldValues[1];

                    if (fieldValues[28].Equals(String.Empty))
                        cmd.Parameters["@EMailAddress"].Value = DBNull.Value;
                    else
                        cmd.Parameters["@EMailAddress"].Value = fieldValues[28];

                    //29 MOBILE PHONE to 36 SMS OTP 20170410
                    if (fieldValues[36].Equals(String.Empty))
                        cmd.Parameters["@MobileNumber"].Value = DBNull.Value;
                    else
                        cmd.Parameters["@MobileNumber"].Value = fieldValues[36];


                    if (fieldValues[30].Equals(String.Empty))
                    {
                        cmd.Parameters["@PreferredLanguage"].Value = DBNull.Value;
                    }
                    else
                    {
                        cmd.Parameters["@PreferredLanguage"].Value = fieldValues[30];
                    }


                    if (fieldValues[15].Equals(String.Empty))
                    {
                        cmd.Parameters["@AcctStatus"].Value = DBNull.Value;
                    }
                    else
                    {
                        cmd.Parameters["@AcctStatus"].Value = fieldValues[15];
                    }


                    if (fieldValues[16].Equals(String.Empty))
                    {
                        cmd.Parameters["@AcctStatusLastUpdDt"].Value = DBNull.Value;
                    }
                    else
                    {
                        cmd.Parameters["@AcctStatusLastUpdDt"].Value = DateTime.ParseExact(fieldValues[16], "dd/MM/yyyy HH:mm:ss", culture);
                    }
                    cmd.ExecuteNonQuery();
                    lineCount++;
                }

                lineData = sr.ReadLine();
                if (lineData.Length < 26 || !lineData.Substring(0, 25).Equals("Total Number of Record = ") || int.Parse(lineData.Substring(25)) != lineCount)
                {
                    tran.Rollback();
                    Console.WriteLine("Invalid IB Trailer Count");
                    return 3;
                }

                tran.Commit();
                conn.Close();
                conn = null;

                sr.Close();
                sr = null;

                String backupPath = ConfigurationSettings.AppSettings["IBFileBackupPath"];

                foreach (String fullfilename in ibFilenames)
                {
                    String backupFilename = Path.Combine(backupPath, DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Path.GetFileName(fullfilename));

                    File.Move(fullfilename, backupFilename);
                }

                Console.WriteLine("Import completed successfully");
                return 0;
            }
            finally
            {
                if (conn != null && (conn.State & ConnectionState.Open) != 0)
                    conn.Close();

                if (sr != null)
                    sr.Close();
            }
        }
    }
}
