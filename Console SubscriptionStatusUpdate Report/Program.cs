﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Console_SubscriptionStatusUpdate_Report
{
    class Program
    {
        static private String delimiter = "||";
        static Boolean error = false;
        static private String SubscriptionStatusUpdateOutputPath = ConfigurationManager.AppSettings.Get("SubscriptionStatusUpdateOutputPath") + "\\";
        static private String SubscriptionStatusUpdateLogFilePath = ConfigurationManager.AppSettings.Get("SubscriptionStatusUpdateLogFilePath") + "\\" + ConfigurationManager.AppSettings.Get("SubscriptionStatusUpdateLogFileName");
        //static private String misStartDate = ConfigurationManager.AppSettings.Get("misStartDate");
        //static private String misEndDate = ConfigurationManager.AppSettings.Get("misEndDate");
        static String genfileExt = "";
        static String genfileExtLog = "";
        static String Text = "";

        static void Main(string[] args)
        {
            error = false;
            //genfileExt = DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            genfileExt = ".csv";
            //genfileExtLog = DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            genfileExtLog = ".txt";
            //DateTime startDate = DateTime.MinValue;//dtpStart.Value;
            //DateTime endDate = DateTime.MaxValue;//dtpEnd.Value;
            DateTime startDate;
            DateTime endDate;
            Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console SubscriptionStatusUpdate Report Start"));

            System.IO.StreamWriter logfile = new System.IO.StreamWriter(SubscriptionStatusUpdateLogFilePath + genfileExtLog, true, Encoding.UTF8);

            endDate = DateTime.Now;
            endDate = endDate.Date;
            startDate = endDate.AddDays(-1);
            endDate = endDate.AddSeconds(-1);

            if (args.Length == 2)
            {
                
                if (DateTime.TryParseExact(args[0],
                               "yyyyMMdd",
                               System.Globalization.CultureInfo.InvariantCulture,
                               System.Globalization.DateTimeStyles.None,
                               out startDate))
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "SubscriptionStatusUpdate Start Date for Report: " + startDate);
                }
                else
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid SubscriptionStatusUpdate Start Date format for Report ");
                    logfile.Write(Text);
                    logfile.Close();
                    Console.WriteLine(Text);
                    Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console SubscriptionStatusUpdate Report End"));
                    System.Threading.Thread.Sleep(3000);
                    return;
                }

                if (DateTime.TryParseExact(args[1],
                               "yyyyMMdd",
                               System.Globalization.CultureInfo.InvariantCulture,
                               System.Globalization.DateTimeStyles.None,
                               out endDate))
                {
                    endDate = endDate.AddDays(1).AddSeconds(-1);
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "SubscriptionStatusUpdate End Date for Report: " + endDate);

                }
                else
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid SubscriptionStatusUpdate End Date format for Report ");
                    logfile.Write(Text);
                    logfile.Close();
                    Console.WriteLine(Text);
                    Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console SubscriptionStatusUpdate Report End"));
                    System.Threading.Thread.Sleep(3000);
                    return;
                }

            }
            else if (args.Length == 1)
            {
                if (DateTime.TryParseExact(args[0],
                               "yyyyMMdd",
                               System.Globalization.CultureInfo.InvariantCulture,
                               System.Globalization.DateTimeStyles.None,
                               out startDate))
                {
                    endDate = startDate.AddDays(1).AddSeconds(-1);
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "SubscriptionStatusUpdate Start Date for Report: " + startDate);
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "SubscriptionStatusUpdate End Date for Report: " + endDate);
                }
                else
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid SubscriptionStatusUpdate Start Date format for Report ");
                    logfile.Write(Text);
                    logfile.Close();
                    Console.WriteLine(Text);
                    Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console SubscriptionStatusUpdate Report End"));
                    System.Threading.Thread.Sleep(3000);
                    return;
                }
            }
            else if (args.Length == 0)
            {
                // Last Month Report
                //date = date.AddMonths(-1);
                //int days = DateTime.DaysInMonth(date.Year, date.Month);
                //startDate = new DateTime(date.Year, date.Month, 1);
                //endDate = new DateTime(date.Year, date.Month, days);
                //endDate = endDate.AddDays(1).AddSeconds(-1);

                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "SubscriptionStatusUpdate Start Date for Report: " + startDate);
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "SubscriptionStatusUpdate End Date for Report: " + endDate);
            }
            else 
            {
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid Input Arguements format.");
                logfile.Write(Text);
                logfile.Close();
                Console.WriteLine(Text);
                Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console SubscriptionStatusUpdate Report End"));
                System.Threading.Thread.Sleep(3000);
                return;
            }
            Int32 noOfRecord = 0;

            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Start Generate SubscriptionStatusUpdate Report");
            noOfRecord = investmentReport(startDate, endDate);//saveFileDialog1.FileName);
            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Finish Generate SubscriptionStatusUpdate Report");
            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   No. of record:{1} has been generated", Text, noOfRecord);
            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Please get the report on '{1}'", Text, SubscriptionStatusUpdateOutputPath + ConfigurationManager.AppSettings.Get("SubscriptionStatusUpdateOutputName") + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + genfileExt); 
            logfile.Write(Text);
            logfile.Close();


            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Please get the log on '{1}'", Text, SubscriptionStatusUpdateLogFilePath + genfileExtLog);

            if (error)
            {
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Generate SubscriptionStatusUpdate Report Completed with ERRORS", Text);
            }
            else
            {
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Generate SubscriptionStatusUpdate Report Completed", Text);
            }
            Console.WriteLine(Text);
            Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program Console SubscriptionStatusUpdate Report End"));
            System.Threading.Thread.Sleep(5000);
        }


        static private Int32 investmentReport(DateTime startDate, DateTime endDate)//, String fileName)
        {
            String csvSubscriptionStatusUpdateData = "";
            Int32 noOfRecord = 0;
            DateTime actionDateTime;
            System.IO.StreamWriter file1 = new System.IO.StreamWriter(SubscriptionStatusUpdateOutputPath + ConfigurationManager.AppSettings.Get("SubscriptionStatusUpdateOutputName") + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + genfileExt, false, Encoding.UTF8);
            try
            {
                using (EstatementEntities entities = new EstatementEntities())
                {
                    var SubscriptionStatusUpdateData = entities.tbSubscriptionStatusUpdates.Where(c => ((DateTime)c.ActionDateTime).CompareTo(startDate) >= 0 && ((DateTime)c.ActionDateTime).CompareTo(endDate) <= 0);
                    //using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, false, Encoding.Unicode))
                    //{
                    #region " Header "
                    csvSubscriptionStatusUpdateData = @"""Action Date Time""";
                    csvSubscriptionStatusUpdateData = csvSubscriptionStatusUpdateData + delimiter + @"""RMID""";
                    csvSubscriptionStatusUpdateData = csvSubscriptionStatusUpdateData + delimiter + @"""Account Type""";
                    csvSubscriptionStatusUpdateData = csvSubscriptionStatusUpdateData + delimiter + @"""Account Number""";
                    csvSubscriptionStatusUpdateData = csvSubscriptionStatusUpdateData + delimiter + @"""Action""";

                    file1.WriteLine(csvSubscriptionStatusUpdateData);
                    #endregion

                    #region " Body "
                    foreach (var item in SubscriptionStatusUpdateData)
                    {

                       
                        DateTime.TryParse(item.ActionDateTime.ToString(), out actionDateTime);

                        csvSubscriptionStatusUpdateData = "\"" + actionDateTime.ToString("yyyyMMddHHmmss");      //Action Date Time
                        csvSubscriptionStatusUpdateData = csvSubscriptionStatusUpdateData + "\"" + delimiter + "\"" + (trimming(item.RMID)).Trim();      //RMID
                        csvSubscriptionStatusUpdateData = csvSubscriptionStatusUpdateData + "\"" + delimiter + "\"" + (trimming(item.AccountType)).Trim();      //Account Type
                        csvSubscriptionStatusUpdateData = csvSubscriptionStatusUpdateData + "\"" + delimiter + "\"" + (trimming(item.AccountNumber)).Trim();      //Account Number
                        csvSubscriptionStatusUpdateData = csvSubscriptionStatusUpdateData + "\"" + delimiter + "\"" + (trimming(item.Action)).Trim();      //Action

                        file1.WriteLine(csvSubscriptionStatusUpdateData + "\"");
                        noOfRecord++;
                    }

                    #endregion
                    //}
                }
            }
            catch (Exception e)
            {
                error = true;
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Error Generate SubscriptionStatusUpdate Report Error: " + e.ToString() + ((e.InnerException == null) ? "" : " (InnerException: " + e.InnerException.ToString() + ")"));
            }
            file1.Close();
            return noOfRecord;
        }

        static private String trimming(String str)
        {
            if (String.IsNullOrEmpty(str))
                return "";
            else
                return str.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
        }


    }
}
