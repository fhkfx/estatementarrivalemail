﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.EntityClient;
using System.Data.Common;
using System.IO;

namespace ConsoleMIS_report
{
    public partial class frmMain : Form
    {
        private String delimiter = ",";
        private String misEmailOutputPath = ConfigurationManager.AppSettings.Get("misEmailOutputPath") + "\\";
        private String misSMSOutputPath = ConfigurationManager.AppSettings.Get("misSMSOutputPath") + "\\";
        private String misLogFilePath = ConfigurationManager.AppSettings.Get("misLogFilePath") + "\\" + ConfigurationManager.AppSettings.Get("misLogFileName");

        String genfileExt = "";
        String genfileExtLog = "";
        Boolean error = false;

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnGen_Click(object sender, EventArgs e)
        {
            error = false;
            //genfileExt = DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            genfileExt = ".csv";
            //genfileExtLog = DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            genfileExtLog = ".txt";
            DateTime startDate = DateTime.MinValue;//dtpStart.Value;
            DateTime endDate = DateTime.MaxValue;//dtpEnd.Value;
            Int32 noOfRecord = 0;

            tbLog.Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", tbLog.Text, "Start Generate MIS Report (Investment)");
            noOfRecord = investmentReport(startDate, endDate);//saveFileDialog1.FileName);
            tbLog.Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", tbLog.Text, "Finish Generate MIS Report (Investment)");           
            tbLog.Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   No. of record:{1} has been generated", tbLog.Text, noOfRecord);
            tbLog.Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Please get the report on '{1}' and '{2}'", tbLog.Text, misEmailOutputPath + DateTime.Now.ToString("yyyyMMdd") + "_" + ConfigurationManager.AppSettings.Get("misEmailOutputName") + genfileExt, misSMSOutputPath + DateTime.Now.ToString("yyyyMMdd") + "_" + ConfigurationManager.AppSettings.Get("misSMSOutputName") + genfileExt);
            System.IO.StreamWriter logfile = new System.IO.StreamWriter(misLogFilePath + genfileExtLog, false, Encoding.Unicode); 
            logfile.Write(tbLog.Text);        
            logfile.Close();


            tbLog.Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Please get the log on '{1}'", tbLog.Text, misLogFilePath + " - " + genfileExtLog);
                            
            if (error)        
            {    
                tbLog.Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Generate MIS Report Completed with ERRORS", tbLog.Text);      
            }        
            else          
            {    
                tbLog.Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Generate MIS Report Completed", tbLog.Text);          
            }       
        }


        private Int32 investmentReport(DateTime startDate, DateTime endDate)//, String fileName)
        {
            String csvEMailData = "";
            String csvSMSData = "";
            Int32 noOfRecord = 0;
            System.IO.StreamWriter file1 = new System.IO.StreamWriter(misEmailOutputPath + DateTime.Now.ToString("yyyyMMdd") + "_" + ConfigurationManager.AppSettings.Get("misEmailOutputName") + genfileExt, false, Encoding.Unicode);
            System.IO.StreamWriter file2 = new System.IO.StreamWriter(misSMSOutputPath + DateTime.Now.ToString("yyyyMMdd") + "_" + ConfigurationManager.AppSettings.Get("misSMSOutputName") + genfileExt, false, Encoding.Unicode);
            try
            {
                using (EstatementEntities entities = new EstatementEntities())
                {
                    var misEMailData = entities.tbMISReportEmails.Where(c => ((DateTime)c.StatementSentDate).CompareTo(startDate) >= 0 && ((DateTime)c.StatementSentDate).CompareTo(endDate) <= 0);
                    var misSMSData = entities.tbMISReportSMS.Where(c => ((DateTime)c.SMSSentDateTime).CompareTo(startDate) >= 0 && ((DateTime)c.SMSSentDateTime).CompareTo(endDate) <= 0);
                    
                    //using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, false, Encoding.Unicode))
                    //{
                    #region " Header "
                    csvEMailData = @"""Statement ID""";
                    csvEMailData = csvEMailData + delimiter + @"""Statement Type""";
                    csvEMailData = csvEMailData + delimiter + @"""RMID""";
                    csvEMailData = csvEMailData + delimiter + @"""Account Number""";
                    csvEMailData = csvEMailData + delimiter + @"""Email Address""";
                    csvEMailData = csvEMailData + delimiter + @"""Statement Sent Date Time""";
                    csvEMailData = csvEMailData + delimiter + @"""Failure Report Date Time""";
                    csvEMailData = csvEMailData + delimiter + @"""Failure Report Subject""";
                    csvEMailData = csvEMailData + delimiter + @"""Hard or Soft Bounce""";
                    csvEMailData = csvEMailData + delimiter + @"""Mobile Number""";
                    csvEMailData = csvEMailData + delimiter + @"""Preferred Language""";

                    csvSMSData = @"""RMID""";
                    csvSMSData = csvSMSData + delimiter + @"""Email Address""";
                    csvSMSData = csvSMSData + delimiter + @"""Bounce Back Count""";
                    csvSMSData = csvSMSData + delimiter + @"""Mobile Number""";
                    csvSMSData = csvSMSData + delimiter + @"""Preferred Language""";
                    csvSMSData = csvSMSData + delimiter + @"""SMS Sent Date Time""";


                    file1.WriteLine(csvEMailData);
                    file2.WriteLine(csvSMSData);
                    #endregion

                    #region " Body "
                    foreach (var item in misEMailData)
                    {

                        csvEMailData = "\"" + item.StatementID;      //Statement ID
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.StatementType);      //Statement Type
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.RMID);      //RMID
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.AccountNumber);      //Account Number
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.EmailAddress);      //Email Address
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + item.StatementSentDate;      //Statement Sent Date Time
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + item.FailureReportDateTime;      //Failure Report Date Time
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.FailureReportSubject);      //Failure Report Subject
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.HardSoftBounce);      //Hard or Soft Bounce
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.MobileNumber);      //Mobile Number
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.PreferredLanguage);      //Preferred Language

                        file1.WriteLine(csvEMailData);
                        noOfRecord++;
                    }

                    foreach (var item in misSMSData)
                    {

                        csvSMSData = "\"" + trimming(item.RMID);      //RMID
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + trimming(item.EmailAddress);      //Email Address
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + item.BounceBackCount;      //Bounce Back Count
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + trimming(item.MobileNumber);      //Mobile Number
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + trimming(item.PreferredLanguage);      //Preferred Language
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + item.SMSSentDateTime;      //SMS Sent Date Time

                        file2.WriteLine(csvSMSData);
                        noOfRecord++;
                    }

                    #endregion
                    //}
                }
            }
            catch (Exception e)
            {
                error = true;
                tbLog.Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", tbLog.Text, "Error Generate MIS Report Error: " + e.ToString() + ((e.InnerException == null)? "" : " (InnerException: " + e.InnerException.ToString() + ")"));
            }
            file1.Close();
            file2.Close();

            return noOfRecord;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private String trimming(String str)
        {
            if (String.IsNullOrEmpty(str))
                return "";
            else
                return str.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
        }
    }
}
