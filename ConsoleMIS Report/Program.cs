﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;



namespace ConsoleMIS_Report
{
    class Program
    {
        static private String delimiter = ",";
        static Boolean error = false;
        static private String misEmailOutputPath = ConfigurationManager.AppSettings.Get("misEmailOutputPath") + "\\";
        static private String misSMSOutputPath = ConfigurationManager.AppSettings.Get("misSMSOutputPath") + "\\";
        static private String misLogFilePath = ConfigurationManager.AppSettings.Get("misLogFilePath") + "\\" + ConfigurationManager.AppSettings.Get("misLogFileName");
        //static private String misStartDate = ConfigurationManager.AppSettings.Get("misStartDate");
        //static private String misEndDate = ConfigurationManager.AppSettings.Get("misEndDate");
        static String genfileExt = "";
        static String genfileExtLog = "";
        static String Text = "";

        static void Main(string[] args)
        {
            error = false;
            //genfileExt = DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            genfileExt = ".csv";
            //genfileExtLog = DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            genfileExtLog = ".txt";
            //DateTime startDate = DateTime.MinValue;//dtpStart.Value;
            //DateTime endDate = DateTime.MaxValue;//dtpEnd.Value;
            DateTime startDate;
            DateTime endDate;
            Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program ConsoleMIS Report Start"));

            System.IO.StreamWriter logfile = new System.IO.StreamWriter(misLogFilePath + genfileExtLog, true, Encoding.Unicode);

            endDate = DateTime.Now;
            endDate = endDate.Date;
            startDate = endDate.AddDays(-1);
            endDate = endDate.AddSeconds(-1);

            if (args.Length == 2)
            {
                
                if (DateTime.TryParseExact(args[0],
                               "yyyyMMdd",
                               System.Globalization.CultureInfo.InvariantCulture,
                               System.Globalization.DateTimeStyles.None,
                               out startDate))
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "MIS Start Date for Report: " + startDate);
                }
                else
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid MIS Start Date format for Report ");
                    logfile.Write(Text);
                    logfile.Close();
                    Console.WriteLine(Text);
                    Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program ConsoleMIS Report End"));
                    System.Threading.Thread.Sleep(3000);
                    return;
                }

                if (DateTime.TryParseExact(args[1],
                               "yyyyMMdd",
                               System.Globalization.CultureInfo.InvariantCulture,
                               System.Globalization.DateTimeStyles.None,
                               out endDate))
                {
                    endDate = endDate.AddDays(1).AddSeconds(-1);
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "MIS End Date for Report: " + endDate);

                }
                else
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid MIS End Date format for Report ");
                    logfile.Write(Text);
                    logfile.Close();
                    Console.WriteLine(Text);
                    Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program ConsoleMIS Report End"));
                    System.Threading.Thread.Sleep(3000);
                    return;
                }

            }
            else if (args.Length == 1)
            {
                if (DateTime.TryParseExact(args[0],
                               "yyyyMMdd",
                               System.Globalization.CultureInfo.InvariantCulture,
                               System.Globalization.DateTimeStyles.None,
                               out startDate))
                {
                    endDate = startDate.AddDays(1).AddSeconds(-1);
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "MIS Start Date for Report: " + startDate);
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "MIS End Date for Report: " + endDate);
                }
                else
                {
                    Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid MIS Start Date format for Report ");
                    logfile.Write(Text);
                    logfile.Close();
                    Console.WriteLine(Text);
                    Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program ConsoleMIS Report End"));
                    System.Threading.Thread.Sleep(3000);
                    return;
                }
            }
            else if (args.Length == 0)
            {

                DateTime date = DateTime.Now;
                date = date.AddMonths(-1);
                int days = DateTime.DaysInMonth(date.Year, date.Month);

                startDate = new DateTime(date.Year, date.Month, 1);
                endDate = new DateTime(date.Year, date.Month, days);
                endDate = endDate.AddDays(1).AddSeconds(-1);

                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "MIS Start Date for Report: " + startDate);
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "MIS End Date for Report: " + endDate);
            }
            else 
            {
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Invalid Input Arguements format.");
                logfile.Write(Text);
                logfile.Close();
                Console.WriteLine(Text);
                Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program ConsoleMIS Report End"));
                System.Threading.Thread.Sleep(3000);
                return;
            }
            Int32 noOfRecord = 0;
            
            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Start Generate MIS Report (Investment)");
            noOfRecord = investmentReport(startDate, endDate);//saveFileDialog1.FileName);
            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Finish Generate MIS Report (Investment)");
            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   No. of record:{1} has been generated", Text, noOfRecord);
            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Please get the report on '{1}' and '{2}'", Text, misEmailOutputPath + DateTime.Now.ToString("yyyyMMdd") + "_" + ConfigurationManager.AppSettings.Get("misEmailOutputName") + genfileExt, misSMSOutputPath + DateTime.Now.ToString("yyyyMMdd") + "_" + ConfigurationManager.AppSettings.Get("misSMSOutputName") + genfileExt);
            logfile.Write(Text);
            logfile.Close();


            Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Please get the log on '{1}'", Text, misLogFilePath + " - " + genfileExtLog);

            if (error)
            {
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Generate MIS Report Completed with ERRORS", Text);
            }
            else
            {
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   Generate MIS Report Completed", Text);
            }
            Console.WriteLine(Text);
            Console.WriteLine(String.Format("[" + DateTime.Now.ToString(@"MM\/dd\/yyyy hh\:mm tt") + "]" + " [Info] Program ConsoleMIS Report End"));
            System.Threading.Thread.Sleep(5000);
        }


        static private Int32 investmentReport(DateTime startDate, DateTime endDate)//, String fileName)
        {
            String csvEMailData = "";
            String csvSMSData = "";
            Int32 noOfRecord = 0;
            System.IO.StreamWriter file1 = new System.IO.StreamWriter(misEmailOutputPath + DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + ConfigurationManager.AppSettings.Get("misEmailOutputName") + genfileExt, false, Encoding.Unicode);
            System.IO.StreamWriter file2 = new System.IO.StreamWriter(misSMSOutputPath + DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + ConfigurationManager.AppSettings.Get("misSMSOutputName") + genfileExt, false, Encoding.Unicode);
            try
            {
                using (EstatementEntities entities = new EstatementEntities())
                {
                    var misEMailData = entities.tbMISReportEmails.Where(c => ((DateTime)c.StatementSentDate).CompareTo(startDate) >= 0 && ((DateTime)c.StatementSentDate).CompareTo(endDate) <= 0);
                    var misSMSData = entities.tbMISReportSMS.Where(c => ((DateTime)c.SMSSentDateTime).CompareTo(startDate) >= 0 && ((DateTime)c.SMSSentDateTime).CompareTo(endDate) <= 0);

                    //using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, false, Encoding.Unicode))
                    //{
                    #region " Header "
                    csvEMailData = @"""Statement ID""";
                    csvEMailData = csvEMailData + delimiter + @"""Statement Type""";
                    csvEMailData = csvEMailData + delimiter + @"""RMID""";
                    csvEMailData = csvEMailData + delimiter + @"""Account Number""";
                    csvEMailData = csvEMailData + delimiter + @"""Email Address""";
                    csvEMailData = csvEMailData + delimiter + @"""Statement Sent Date Time""";
                    csvEMailData = csvEMailData + delimiter + @"""Failure Report Date Time""";
                    csvEMailData = csvEMailData + delimiter + @"""Failure Report Subject""";
                    csvEMailData = csvEMailData + delimiter + @"""Hard or Soft Bounce""";
                    csvEMailData = csvEMailData + delimiter + @"""Mobile Number""";
                    csvEMailData = csvEMailData + delimiter + @"""Preferred Language""";
                    csvEMailData = csvEMailData + delimiter + @"""UNE Reference Number""";

                    csvSMSData = @"""RMID""";
                    csvSMSData = csvSMSData + delimiter + @"""Email Address""";
                    csvSMSData = csvSMSData + delimiter + @"""Bounce Back Count""";
                    csvSMSData = csvSMSData + delimiter + @"""Mobile Number""";
                    csvSMSData = csvSMSData + delimiter + @"""Preferred Language""";
                    csvSMSData = csvSMSData + delimiter + @"""SMS Sent Date Time""";
                    csvSMSData = csvSMSData + delimiter + @"""UNE Reference Number""";


                    file1.WriteLine(csvEMailData);
                    file2.WriteLine(csvSMSData);
                    #endregion

                    #region " Body "
                    foreach (var item in misEMailData)
                    {

                        csvEMailData = "\"" + item.StatementID;      //Statement ID
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.StatementType);      //Statement Type
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.RMID);      //RMID
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.AccountNumber);      //Account Number
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.EmailAddress);      //Email Address
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + item.StatementSentDate;      //Statement Sent Date Time
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + item.FailureReportDateTime;      //Failure Report Date Time
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.FailureReportSubject);      //Failure Report Subject
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.HardSoftBounce);      //Hard or Soft Bounce
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.MobileNumber);      //Mobile Number
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.PreferredLanguage);      //Preferred Language
                        csvEMailData = csvEMailData + "\"" + delimiter + "\"" + trimming(item.UNEReferenceNumber);      //UNE Reference Number

                        file1.WriteLine(csvEMailData + "\"");
                        noOfRecord++;
                    }

                    foreach (var item in misSMSData)
                    {

                        csvSMSData = "\"" + trimming(item.RMID);      //RMID
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + trimming(item.EmailAddress);      //Email Address
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + item.BounceBackCount;      //Bounce Back Count
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + trimming(item.MobileNumber);      //Mobile Number
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + trimming(item.PreferredLanguage);      //Preferred Language
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + item.SMSSentDateTime;      //SMS Sent Date Time
                        csvSMSData = csvSMSData + "\"" + delimiter + "\"" + item.UNEReferenceNumber;      //UNE Reference Number

                        file2.WriteLine(csvSMSData + "\"");
                        noOfRecord++;
                    }

                    #endregion
                    //}
                }
            }
            catch (Exception e)
            {
                error = true;
                Text = String.Format("{0}\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   {1}", Text, "Error Generate MIS Report Error: " + e.ToString() + ((e.InnerException == null) ? "" : " (InnerException: " + e.InnerException.ToString() + ")"));
            }
            file1.Close();
            file2.Close();

            return noOfRecord;
        }

        static private String trimming(String str)
        {
            if (String.IsNullOrEmpty(str))
                return "";
            else
                return str.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
        }


    }
}
